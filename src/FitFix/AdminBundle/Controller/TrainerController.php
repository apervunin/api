<?php

namespace FitFix\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FitFix\CoreBundle\Entity\Trainer;
use FitFix\AdminBundle\Form\TrainerType;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Trainer controller.
 *
 * @Route("/admin/trainer")
 */
class TrainerController extends Controller
{
    /**
     * Lists all Trainer entities.
     *
     * @Route("/", name="admin_trainer")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('FitFixCoreBundle:Trainer')->findAll();

        return array(
            'entities' => $entities,
        );
    }


    /**
     * Finds and displays a Trainer entity.
     *
     * @Route("/{id}/show", name="admin_trainer_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FitFixCoreBundle:Trainer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trainer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new Trainer entity.
     *
     * @Route("/new", name="admin_trainer_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Trainer();
        $form   = $this->createForm(new TrainerType('add'), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a new Trainer entity.
     *
     * @Route("/create", name="admin_trainer_create")
     * @Method("POST")
     * @Template("FitFixAdminBundle:Trainer:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Trainer();
        $form = $this->createForm(new TrainerType('add'), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_trainer_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Trainer entity.
     *
     * @Route("/{id}/edit", name="admin_trainer_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FitFixCoreBundle:Trainer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trainer entity.');
        }

        $editForm = $this->createForm(new TrainerType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Trainer entity.
     *
     * @Route("/{id}/update", name="admin_trainer_update")
     * @Method("POST")
     * @Template("FitFixAdminBundle:Trainer:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FitFixCoreBundle:Trainer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trainer entity.');
        }

        $clientIds = array();
        $clientEntities = array();
        // store existing clients to compare for removal
        foreach ($entity->getClients() as $client) {
            $clientIds[] = $client->getId();
            $clientEntities[] = $client;
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new TrainerType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {

            $updatedClientIds = array();
            // add clients
            foreach ($entity->getClients() as $client) {
                $updatedClientIds[] = $client->getId();
                if (!in_array($client->getId(), $clientIds)) {
                    $client->addTrainer($entity);
                    $em->persist($client);
                }
            }

            // remove clients
            foreach ($clientEntities as $client) {
                // compare with updated clients to check for removal
                if (!in_array($client->getId(), $updatedClientIds)) {
                    $client->removeTrainer($entity);
                    $em->persist($client);
                }
            }

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_trainer_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Trainer entity.
     *
     * @Route("/{id}/delete", name="admin_trainer_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FitFixCoreBundle:Trainer')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Trainer entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_trainer'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
