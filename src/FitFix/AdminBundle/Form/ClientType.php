<?php

namespace FitFix\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClientType extends AbstractType
{
    public function __construct($mode = 'update') {
        $this->mode = $mode;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', new UserType())
            ->add('gender', 'choice', array(
                'choices' => array('male' => 'Male', 'female' => 'Female')
            ))
            ->add('mobile')
            ->add('dob')
            ->add('firstName')
            ->add('lastName')
            ->add('trainer', 'entity', array(
                'class' => 'FitFixCoreBundle:Trainer',
                'multiple' => true
            ))
            ->add('reviews', 'collection', array(
                'type' => new ReviewType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options'  => array(
                    'label' => 'review',
                    'attr'      => array('class' => 'review-fields-list')
                ),
            ))
            ->add('goals', 'collection', array(
                'type' => new GoalType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options'  => array(
                    'label' => 'goal',
                    'attr'      => array('class' => 'goal-fields-list')
                ),
            ))
            ->add('objectives', 'collection', array(
                'type' => new ObjectiveType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options'  => array(
                    'label' => 'objective',
                    'attr'      => array('class' => 'objective-fields-list')
                ),
            ))
            ->add('mealplans', 'collection', array(
                'type' => new MealType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options'  => array(
                    'label' => 'mealplans',
                    'attr'      => array('class' => 'meal-fields-list')
                ),
            ))
            ->add('workouts', 'collection', array(
                'type' => new WorkoutType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options'  => array(
                    'label' => 'workout',
                    'attr'      => array('class' => 'workout-fields-list')
                ),
            ))
            ->add('notes', 'collection', array(
                'type' => new NoteType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'options'  => array(
                    'label' => 'note',
                    'attr'      => array('class' => 'note-fields-list')
                ),
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        if ($this->mode == 'add') {
            $validationGroups = array('Registration', 'all');
        }
        elseif ($this->mode == 'update') {
            $validationGroups = array('Profile', 'all');
        }

        $resolver->setDefaults(array(
            'data_class' => 'FitFix\CoreBundle\Entity\Client',
            'validation_groups' => $validationGroups
        ));
    }

    public function getName()
    {
        return 'fitfix_adminbundle_clienttype';
    }
}
