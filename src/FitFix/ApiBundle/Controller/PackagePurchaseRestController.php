<?php 

namespace FitFix\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FitFix\CoreBundle\Entity\Invoice;
use FitFix\CoreBundle\Entity\Client;
use FitFix\CoreBundle\Entity\PackagePurchase;
use FitFix\CoreBundle\Form\PackagePurchaseType;
use Buzz\Exception\RuntimeException;
use FOS\RestBundle\Controller\FOSRestController;
use FitFix\CoreBundle\Entity\Package;
use FitFix\CoreBundle\Repository\PackagePurchaseRepository;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializationContext;
use FitFix\CoreBundle\Entity\Session;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author gavinwilliams
 *
 * Used to purchase packages from a PT
 * 
 * @NamePrefix("fitfix_api_packagepurchaserest_")
 *
 * - If a Client books a session against a package, it counts towards the number of available sessions for that package until it has been declined or cancelled - DONE
 * - If a PT/Client accepts a session associated with a package, the number of available sessions decreases - DONE
 * - If a PT/Client declines a session associated with a package, the number of available sessions increases - DONE
 * - The number of available sessions should be returned by the API - DONE
 * - The sessions associated with the package should be returned by the API - DONE
 * - A package can be assigned to a client by reference - DONE
 * 
 * POST
 * 	Requires a package ID, creates an invoice on creation - DONE
 * 
 * POST Session
 * 	Requires the package ID and session data (will take the ID from the session and add a reference) - DONE
 * 
 * GET
 * 	Returns the package purchase, the package, number of calculated sessions remaining and associated sessions. Only returns packages that have been paid for - DONE
 * 
 * PUT
 * 	Unavailable
 * 
 * DELETE
 * 	Deletes the purchase record NOT the invoice - DONE
 */
class PackagePurchaseRestController extends FOSRestController {
	
	/**
	 * Deletes a package purchase for a client
	 * 
	 * @ApiDoc(
	 *	section="Package Purchase",
	 * 	resource="Package Purchase"
	 * )
	 * 
	 * @Secure(roles="ROLE_CLIENT")
	 * 
	 * @ParamConverter("packagepurchase", class="FitFixCoreBundle:PackagePurchase")
	 * 
	 * @param PackagePurchase $packagepurchase
	 * @throws HttpException
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function deletePackagepurchaseAction(PackagePurchase $packagepurchase){
		
		// Check to see whether this client belongs to the package purchase
		if($packagepurchase->getClient()->getId() !== $this->getUser()->getClient()->getId()){
			throw new HttpException(403, "You do not have permission to delete this package purchase");
		}
		
		// Make sure that there are no sessions
		if($packagepurchase->getSessions()->count() > 0){
			throw new HttpException(403, "You do not have permission to delete this package purchase as there are sessions associated with it");
		}
		
		// Make sure that the invoice hasn't been paid
		if($packagepurchase->getInvoice()->getStatus() == 'paid'){
			throw new HttpException(403, "You do not have permission to delete this package purchase as invoice has already been paid");
		}
		
		$em = $this->getDoctrine()->getManager();
		$em->remove($packagepurchase);
		$em->flush();

		$view = $this->view(null, 204);
		
		return $view;
	}
	
	/**
	 * Deletes a package purchase for a trainers client
	 *
	 * @ApiDoc(
	 *	section="Package Purchase",
	 * 	resource="Package Purchase"
	 * )
	 *
	 * @Secure(roles="ROLE_TRAINER")
	 *
	 * @ParamConverter("client", class="FitFixCoreBundle:Client")
	 * @ParamConverter("packagepurchase", class="FitFixCoreBundle:PackagePurchase")
	 *
	 * @param PackagePurchase $packagepurchase
	 * @throws HttpException
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function deleteClientsPackagepurchaseAction(Client $client, PackagePurchase $packagepurchase){
	
		$trainer = $this->getUser()->getTrainer();
		// Check to see whether this trainer belongs to the package purchase
		if($packagepurchase->getPackage()->getTrainer()->getId() !== $trainer->getId()){
			throw new HttpException(403, "You do not have permission to delete this package purchase");
		}
		
		// Make sure that this client belongs to this PT
		if($client->getTrainer()->getId() !== $trainer->getId()){
			throw new HttpException(403, "You do not have permission to access this client");
		}
	
		// Make sure that there are no sessions
		if($packagepurchase->getSessions()->count() > 0){
			throw new HttpException(403, "You do not have permission to delete this package purchase as there are sessions associated with it");
		}
	
		// Make sure that the invoice hasn't been paid
		if($packagepurchase->getInvoice()->getStatus() == 'paid'){
			throw new HttpException(403, "You do not have permission to delete this package purchase as invoice has already been paid");
		}
	
		$em = $this->getDoctrine()->getManager();
		$em->remove($packagepurchase);
		$em->flush();
	
		$view = $this->view(null, 204);
	
		return $view;
	}
	
	/**
	 * Gets package purchases for a client
	 * 
	 * @ApiDoc(
	 * 	section="Package Purchase",
	 * 	resource="Package Purchase",
	 * 	output="FitFix\CoreBundle\Entity\PackagePurchase"
	 * )
	 * 
	 * @Secure(roles="ROLE_CLIENT")
	 * 
	 * @return Ambigous <\FOS\RestBundle\View\View, \FOS\RestBundle\View\View>
	 */
	public function getPackagepurchasesAction(){
		
		$repo = $this->getDoctrine()->getManager()->getRepository('FitFixCoreBundle:PackagePurchase');
		
		$data = $repo->findByClient($this->getUser()->getClient());
		
		$view = $this->view($data);
		$view->setSerializationContext(SerializationContext::create()->setGroups(array('packagepurchase-list')));
		
		return $view;
		
	}
	
	/**
	 * Gets package purchases for a client from a PT
	 *
	 * @ApiDoc(
	 * 	section="Package Purchase",
	 * 	resource="Package Purchase",
	 * 	output="FitFix\CoreBundle\Entity\PackagePurchase"
	 * )
	 *
	 * @Secure(roles="ROLE_TRAINER")
	 * 
	 * @ParamConverter("client", class="FitFixCoreBundle:Client")
	 *
	 * @return Ambigous <\FOS\RestBundle\View\View, \FOS\RestBundle\View\View>
	 */
	public function getClientsPackagepurchasesAction(Client $client){
		
		$trainer = $this->getUser()->getTrainer();
		// Check to see whether this client belongs to the PT
		if($trainer->getId() !== $client->getTrainer()->getId()){
			throw new HttpException(403, "You do not have permission to access this client");
		}
	
		$repo = $this->getDoctrine()->getManager()->getRepository('FitFixCoreBundle:PackagePurchase');
		/* @var $repo PackagePurchaseRepository */
	
		$data = $repo->findPackagePurchasesForClientFromTrainer($client, $trainer);
	
		$view = $this->view();
				
		$view->setData($data);
		
		$serializationContext = SerializationContext::create()->setGroups(array('packagepurchase-details'));
		
		$view->setSerializationContext($serializationContext);
		
	
		return $this->handleView($view);
	
	}
	
	/**
	 * Gets a package for a client
	 * 
	 * @ApiDoc(
	 * 	section="Package Purchase",
	 * 	resource="Package Purchase",
	 * 	output="FitFix\CoreBundle\Entity\PackagePurchase"
	 * )
	 * 
	 * @Secure(roles="ROLE_CLIENT")
	 * 
	 * @ParamConverter("packagepurchase", class="FitFixCoreBundle:PackagePurchase")
	 * 
	 * @param PackagePurchase $packagepurchase
	 * @throws HttpException
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function getPackagepurchaseAction(PackagePurchase $packagepurchase){
		
		// Check to make sure that this purchase belongs to this client
		if($this->getUser()->getClient()->getId() !== $packagepurchase->getClient()->getId()){
			throw new HttpException(403, "This package does not belong to you");
		}
		
		$view = $this->view($packagepurchase);
		
		$view->setSerializationContext(SerializationContext::create()->setGroups(array('packagepurchase-details')));
		
		return $view;
		
	}
	
	/**
	 * Gets a package purchase for a client from a trainer
	 * 
	 * @ApiDoc(
	 * 	section="Package Purchase",
	 * 	resource="Package Purchase",
	 * 	output="FitFix\CoreBundle\Entity\PackagePurchase"
	 * )
	 * 
	 * @Secure(roles="ROLE_TRAINER")
	 *
	 * @ParamConverter("client", class="FitFixCoreBundle:Client")
	 * @ParamConverter("packagepurchase", class="FitFixCoreBundle:PackagePurchase")
	 * 
	 * @param Client $client
	 * @param PackagePurchase $packagepurchase
	 * @throws HttpException
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function getClientsPackagepurchaseAction(Client $client, PackagePurchase $packagepurchase) {
		
		$trainer = $this->getUser()->getTrainer();
		// Check to make sure that this client belongs to the PT
		if($trainer->getId() !== $client->getTrainer()->getId()){
			throw new HttpException(403);
		}
		
		// Check to see whether the package purchase belongs to the PT
		if($trainer->getId() !== $packagepurchase->getPackage()->getTrainer()->getId()){
			throw new HttpException(403);
		}
		
		$view = $this->view($packagepurchase);
		
		$view->setSerializationContext(SerializationContext::create()->setGroups(array('packagepurchase-details')));
		
		return $view;
	}
	
	/**
	 * Posts a package purchase to the currently logged in client
	 * 
	 * @ApiDoc(
	 * 	section="Package Purchase",
	 * 	resource="Package Purchase",
	 * 	input="FitFix\CoreBundle\Form\PackagePurchaseType",
	 * 	output="FitFix\CoreBundle\Entity\PackagePurchase"
	 * )
	 * 
	 * @Secure(roles="ROLE_CLIENT")
	 * 
	 * @throws HttpException
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function postPackagepurchasesAction(){
				
		$data = $this->getRequestData();
		$em = $this->getDoctrine()->getManager();
		// I need to get the package price to pass to the invoice, this is the only way :(
		$repo = $em->getRepository('FitFixCoreBundle:Package');
		
		$package = $repo->find($data['package']);
		/* @var $package Package */

		if(!$package){
			throw new HttpException(404, "Package does not exist");
		}
		
		// Let's make sure that this client can purchase the PT's package!
		if($package->getTrainer()->getId() !== $this->getUser()->getClient()->getTrainer()->getId()){
			throw new HttpException(403);
		}
		
		// Create the invoice
		$invoice = new Invoice();
		$invoice->setPrice($package->getPrice());
		$invoice->setTag('package');
		$invoice->setDescription($package->getName());
		$invoice->setTo($this->getUser()->getClient());
		$invoice->setFrom($package->getTrainer());
		
		// Looks like the invoice needs to be persisted first!
		$em->persist($invoice);
		
		$packagePurchase = new PackagePurchase();
		$packagePurchase->setInvoice($invoice);
		$packagePurchase->setClient($this->getUser()->getClient());
		
		$view = $this->processForm($packagePurchase);
		$view->setSerializationContext(SerializationContext::create()->setGroups(array('packagepurchase-details')));
		
		return $view;
		
	}
	
	/**
	 * Posts a package purchase to a client for the currently logged in PT
	 *
	 * @ApiDoc(
	 * 	section="Package Purchase",
	 * 	resource="Package Purchase",
	 * 	input="FitFix\CoreBundle\Form\PackagePurchaseType",
	 * 	output="FitFix\CoreBundle\Entity\PackagePurchase"
	 * )
	 *
	 * @Secure(roles="ROLE_TRAINER")
	 * 
	 * @ParamConverter("client", class="FitFixCoreBundle:Client")
	 *
	 * @throws HttpException
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function postClientsPackagepurchasesAction(Client $client){
	
		$trainer = $this->getUser()->getTrainer();

		// Let's make sure that the client is the PT's
		if($client->getTrainer()->getId() !== $trainer->getId()){
			throw new HttpException(403, "You do not have permission to access this client");
		}
		
		$data = $this->getRequestData();
		$em = $this->getDoctrine()->getManager();
		// I need to get the package price to pass to the invoice, this is the only way :(
		$repo = $em->getRepository('FitFixCoreBundle:Package');
	
		$package = $repo->find($data['package']);
		/* @var $package Package */
		
		// Let's make sure that this package belongs to the PT
		if($package->getTrainer()->getId() !== $trainer->getId()){
			throw new HttpException(403, "This is not your package");
		}
	
		// Create the invoice
		$invoice = new Invoice();
		$invoice->setPrice($package->getPrice());
		$invoice->setTag('package');
		$invoice->setDescription($package->getName());
		$invoice->setTo($client);
		$invoice->setFrom($trainer);
	
		// Looks like the invoice needs to be persisted first!
		$em->persist($invoice);
	
		$packagePurchase = new PackagePurchase();
		$packagePurchase->setInvoice($invoice);
		$packagePurchase->setClient($client);
	
		$view = $this->processForm($packagePurchase);
		$view->setSerializationContext(SerializationContext::create()->setGroups(array('packagepurchase-details')));
	
		return $view;
	
	}
	
	
	/**
	 * Posts a session to a package purchase for a client
	 * 
	 * @ApiDoc(
	 * 	resource="Package Purchase",
	 * 	section="Package Purchase",
	 * 	input="FitFix\CoreBundle\Form\SessionType",
	 * 	output="FitFix\CoreBundle\Entity\PackagePurchase"
	 * )
	 * 
	 * @Secure(roles="ROLE_CLIENT")
	 * 
	 * @ParamConverter("packagepurchase", class="FitFixCoreBundle:PackagePurchase")
	 * 
	 * @param PackagePurchase $packagepurchase
	 * @throws HttpException
	 * @throws NotFoundHttpException
	 * @return Ambigous <\Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function postPackagepurchasesSessionsAction(PackagePurchase $packagepurchase){
		
		// Make sure that this package purchase belongs to the currently logged in user!
		if($this->getUser()->getClient() && $packagepurchase->getClient()->getId() !== $this->getUser()->getClient()->getId()){
			throw new HttpException(403);
		}
		
		$em = $this->getDoctrine()->getManager();
		
		// Get the session so I can add it to the package purchase
		$data = $this->getRequestData();

		$session = $em->getRepository('FitFixCoreBundle:Session')->find($data['session']);
		/* @var $session Session */
		
		// Check to make sure that this client can actually assign a session to the package purchase
		if(!$session){
			throw new NotFoundHttpException('Cannot find session');
		}
		
		if($this->getUser()->getClient()){
		
			$sessionClient = ($session->getFrom()->getClient()) ? $session->getFrom()->getClient() :  $session->getTo()->getClient();
			/* @var $sessionClient Client */
				
			if($sessionClient->getId() !== $this->getUser()->getClient()->getId()){
				throw new HttpException(403);
			}
			
		}
		
		try {
			$packagepurchase->addSession($session);
		} catch (RuntimeException $e){
			throw new HttpException(403, "You do not have permission to add more sessions to this package purchase");
		}
		
		$em->persist($packagepurchase);
		$em->flush();
		
		$view = $this->view($packagepurchase);
		
		$view->setSerializationContext(SerializationContext::create()->setGroups(array('packagepurchase-details')));
		
		return $view;
		
	}
	
	/**
	 * Allows a PT to post a session to a client's package purchase
	 * 
	 * @ApiDoc(
	 * 	resource="Package Purchase",
	 * 	section="Package Purchase",
	 * 	input="FitFix\CoreBundle\Form\SessionType",
	 * 	output="FitFix\CoreBundle\Entity\PackagePurchase"
	 * )
	 * 
	 * @Secure(roles="ROLE_TRAINER")
	 * 
	 * @ParamConverter("client", class="FitFixCoreBundle:Client")
	 * @ParamConverter("packagepurchase", class="FitFixCoreBundle:PackagePurchase")
	 * 
	 * @param Client $client
	 * @param PackagePurchase $packagepurchase
	 * @throws HttpException
	 * @return Ambigous <\FitFix\ApiBundle\Controller\Ambigous, \Symfony\Component\HttpFoundation\Response, mixed>
	 */
	public function postClientsPackagepurchasesSessionsAction(Client $client, PackagePurchase $packagepurchase){
		
		// Make sure that this package belongs to the PT
		if($packagepurchase->getPackage()->getTrainer()->getId() !== $this->getUser()->getTrainer()->getId()){
			throw new HttpException(403, "This package does not belong to you");
		}
		
		// And that the Client belongs to this PT
		if($client->getTrainer()->getId() !== $this->getUser()->getTrainer()->getId()){
			throw new HttpException(403, "This client is not associated with you");
		}
		
		$packagepurchase->setClient($client);
		return self::postPackagepurchasesSessionsAction($packagepurchase);
		
	}
	
	/**
	 * Processes a Package Purchase object using a form
	 * @param PackagePurchase $packagePurchase
	 * @return FOS\RestBundle\View
	 */
	private function processForm(PackagePurchase $packagePurchase){
	
		$statusCode = $packagePurchase->getId() ? 204 : 201;
		$view = $this->view();
		$data;

		$form = $this->createForm(new PackagePurchaseType(), $packagePurchase);
	
		$data = $this->getRequestData();
		
		// Remove the ID, it'll break EVERYTHING!
		unset($data['id']);
	
		$form->submit($data, false);
	
		if($form->isValid()){
	
			$em = $this->getDoctrine()->getManager();
	
			$em->persist($packagePurchase);
			$em->flush();
			$view->setStatusCode($statusCode);
			if (201 == $statusCode) {
				/*
					$view->setHeader('Location', $this->generateUrl('fitfix_api_invoicerest_get_invoice', array(
							'invoice' => $invoice->getId(),
							'_format' => $this->getRequest()->get('_format', 'json')
					), true));
				*/
			}
	
			$view->setData($packagePurchase);
	
			return $view;
	
		}
	
		return $view->setData($form, 400);
	
	}
	
	private function getRequestData(){

		$requestBodyType = $this->getRequest()->getContentType();
				
		$data;
		
		if($requestBodyType !== null){
			if($requestBodyType == 'json'){
				$data = json_decode($this->getRequest()->getContent(), true);
				if($data === null){
		
					switch (json_last_error()) {
						default:
							return;
						case JSON_ERROR_DEPTH:
							$error = 'Maximum stack depth exceeded';
							break;
						case JSON_ERROR_STATE_MISMATCH:
							$error = 'Underflow or the modes mismatch';
							break;
						case JSON_ERROR_CTRL_CHAR:
							$error = 'Unexpected control character found';
							break;
						case JSON_ERROR_SYNTAX:
							$error = 'Syntax error, malformed JSON';
							break;
						case JSON_ERROR_UTF8:
							$error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
							break;
					}
			
					throw new RuntimeException(sprintf('Unable to decode JSON with error - %s', $error), 400);
							
				}
			}
		} else {
			$data = $this->getRequest();
		}
		
		return $data;
	}
}