<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\VirtualProperty;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\SecurityExtraBundle\Security\Util\String;
use Doctrine\Common\Collections\Criteria;
use Rhumsaa\Uuid\Uuid;

/**
 * Trainer
 *
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Entity\Repository\TrainerRepository")
 * @ORM\Table(name="trainer")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Trainer
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"trainer-profile", "list", "details", "event-details", "sessiontype-details", "sessiontypelocation-details", "invoice-details", "review-list", "review-details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=45, nullable=false, name="first_name")
     * @Expose
     * @Groups({"list", "details", "event-details", "sessiontype-details", "notification", "invoice-details", "trainer-profile", "dashboard-pt"})
     * @Assert\NotBlank(
     *     message="Please enter your first name",
     *     groups={"Registration", "Profile"}
     * )
     * 
     * @SerializedName("firstName")
     */
    private $firstName;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Expose
     * @Groups({"list", "details", "event-details", "sessiontype-details", "notification", "invoice-details", "trainer-profile"})
     * 
     * @Assert\Email()
     * 
     * @SerializedName("payPalAddress")
     */
    private $payPalAddress;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=6, nullable=true, name="gender")
     * @Expose
     * @Groups({"list", "details", "event-details", "sessiontype-details", "notification", "invoice-details", "trainer-profile","dashboard-pt"})
     * @Assert\Choice(choices={"Male", "Female"})
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=45, nullable=false, name="last_name")
     * @Expose
     * @Groups({"list", "details", "event-details", "sessiontype-details", "notification", "invoice-details", "trainer-profile", "dashboard-pt"})
     * @Assert\NotBlank(
     *     message="Please enter your last name",
     *     groups={"Registration", "Profile"}
     * )
     * @SerializedName("lastName")
     * 
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true, name="bio")
     * @Expose()
     * @Groups({"details", "trainer-profile"})
     * @Assert\NotBlank(
     *     message="Please enter your biography",
     *     groups={"Profile"}
     * )
     */
    private $bio;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Client", mappedBy="trainer", cascade={"persist","remove"})
     * @Expose
     * @Groups({"details"})
     * @Assert\Valid(
     *     traverse=false
     * )
     */
    private $clients;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Mealplan", mappedBy="trainer", cascade={"persist","remove"})
     * @Expose
     * @Groups({"details"})
     * @Assert\Valid(
     *     traverse=false
     * )
     */
    private $mealplans;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Package", mappedBy="trainer", cascade={"persist","remove"})
     * @Expose
     * @Groups({"details"})
     * @Assert\Valid(
     *     traverse=false
     * )
     */
    private $packages;

    /**
     * @ORM\OneToOne(targetEntity="FitFix\CoreBundle\Entity\User", inversedBy="trainer", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", unique=true)
     * @Expose
     * @Groups({"details"})
     * @Assert\Valid(
     *     traverse=false
     * )
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Workout", mappedBy="trainer", cascade={"persist","remove"})
     * @Expose
     * @Groups({"details"})
     * @Assert\Valid(
     *     traverse=false
     * )
     */
    private $workouts;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Invoice", mappedBy="from", cascade={"persist","remove"})
     * @Expose
     * @Groups({"details"})
     * @Assert\Valid(
     *     traverse=false
     * )
     */
    private $invoices;

    /**
     *
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Address", mappedBy="trainer", cascade={"persist", "remove"})
     * @Expose
     * @Groups({"details", "list"})
     */
    private $locations;
    
    /**
     * The PT's environments
     * @var unknown
     * 
     * @ORM\Column(type="json_array", nullable=true)
     * @Expose()
     * @Groups({"trainer-profile"})
     * 
     * @SerializedName("sessionEnvironments")
     */
    private $sessionEnvironments;

    /**
     * The users photo
     * @var string
     * @ORM\Column(name="photo", type="string", nullable=true)
     * @Expose
     * @Groups({"list", "details", "event-details", "trainer-profile", "dashboard-pt"})
     */
    private $photo;
    
    /**
     * The users logo
     * @var unknown
     * @ORM\Column(name="logo", type="string", nullable=true)
     * @Expose()
     * @Groups({"list", "details", "trainer-profile", "dashboard-pt"})
     */
    private $logo;

    /**
     * The date the trainer started training
     * @var \DateTime
     * @ORM\Column(name="experience", type="string", nullable=true)
     * @Expose
     * @Groups({"trainer-profile"})
     */
    private $experience;

    /**
     * The languages that the PT speaks
     * @var array
     * @ORM\Column(name="languages", type="json_array", nullable=true)
     * @Expose
     * @Groups({"trainer-profile"})
     */
    private $languages;

    /**
     * The PT's location
     * @var string
     * @ORM\Column(name="location", type="string", nullable=true)
     * @Expose
     * @Groups({"trainer-profile"})
     */
    private $location;

    /**
     * The PT's specialisms
     * @var array
     * @ORM\Column(name="specialisms", type="json_array", nullable=true)
     * @Expose
     * @Groups({"trainer-profile", "dashboard-pt"})
     */
    private $specialisms;
    
    /**
     * The PT's qualifications
     * @var unknown
     * @ORM\Column(type="json_array", nullable=true)
     * @Expose()
     * @Groups({"trainer-profile"})
     * 
     */
    private $qualifications;
    
    
    /**
     * The PT's local billing currency
     * @var unknown
     * @ORM\Column(type="string", length=20)
     * @Assert\Choice(choices={"GBP:�", "USD:$", "AUS:$", "EUR:�"})
     * @Expose()
     * @Groups({"trainer-profile", "list", "details", "trainer-profile"})
     * 
     * @SerializedName("billingCurrency")
     */
    private $billingCurrency;
    
    

    /**
     * The PT's reviews
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Review", mappedBy="trainer")
     * @Expose
     * @Groups({"trainer-profile"});
     */
    private $reviews;


    /**
     * REPS Verified
     * @var boolean
     * @ORM\Column(type="string", nullable=false)
     * @Expose
     * @Groups({"list", "details", "event-details", "trainer-profile"})
     * @Assert\NotNull()
     * @Assert\Choice(choices={"yes", "no"})
     * 
     * @SerializedName("repsVerified")
     */
    private $repsVerified;
    
    /**
     * The birthday of tiraner
     * @var \DateTime
     * @ORM\Column(name="birthday", type="date", nullable=true)
     * @Expose
     * @Groups({"trainer-profile"})
     */
    private $birthday;
    
    /**
     * The PT's phone number
     * @var unknown
     * @ORM\Column(type="string")
     * @Expose()
     * @Groups({"trainer-profile", "details", "list"})
     * 
     * @SerializedName("phoneNumber")
     */
    private $phoneNumber;
    
    /**
     * Created at
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     * @Expose
     * @Groups({"trainer-profile"})
     * 
     * @SerializedName("createdAt")
     */
    private $createdAt;
    /**
     * lastPaidAt
     * @var \DateTime
     * @ORM\Column(name="last_paid_at", type="datetime", nullable=true)
     * @Expose
     * @Groups({"trainer-profile"})
     * 
     * @SerializedName("lastPaidAt")
     */
    private $lastPaidAt;

    /**
     * lastPaidAmount
     * @ORM\Column(name="last_paid_amount", type="decimal", nullable=true,  scale=2)
     * @Expose
     * @Groups({"trainer-profile"})
     * 
     * @SerializedName("lastPaidAmount")
     */
    private $lastPaidAmount;

    /**
     * expireDate
     * @var \DateTime
     * @ORM\Column(name="expire_date", type="datetime", nullable=true)
     * @Expose
     * @Groups({"trainer-profile"})
     * 
     * @SerializedName("expiredDate")
     */
    private $expireDate;
    
    /**
     * @ORM\Column(type="text")
     * @Expose()
     * @Groups({"trainer-profile", "details", "list"})
     */
    private $terms;
    
    /**
     * @ORM\Column(type="string")
     * @var unknown
     */
    private $addressNumber;
    
    /**
     * @ORM\Column(type="string")
     * @var unknown
     */
    private $addressStreet;
    
    /**
     * @ORM\Column(type="string")
     * @var unknown
     */
    private $addressTown;

    /**
     * @ORM\Column(type="string")
     * @var unknown
     */
    private $addressPostCode;
    
    /**
     * @ORM\Column(type="string")
     * @Assert\Country()
     * @var unknown
     */
    private $addressCountry;
    
    /**
     * @ORM\Column(type="string")
     * @var unknown
     */
    private $uuid;

    /**
     * @ORM\Column(type="string")
     * @var unknown
     */
    private $trial;

    
    public function __toString()
    {
        return sprintf('%s %s (%s)', $this->firstName, $this->lastName, $this->getUser()->getEmail());
    }
	
	
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clients = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mealplans = new \Doctrine\Common\Collections\ArrayCollection();
        $this->packages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->workouts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->invoices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->locations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reviews = new \Doctrine\Common\Collections\ArrayCollection();
        $this->terms = 'Blah Blah Blah';
        
        $this->uuid = Uuid::uuid4()->toString();
        
        $this->logo = "http://fitfixappdata.s3.amazonaws.com/images/defaults/app-logo.png";
        $this->repsVerified = 'no';
        $this->createdAt = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Trainer
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set payPalAddress
     *
     * @param string $payPalAddress
     * @return Trainer
     */
    public function setPayPalAddress($payPalAddress)
    {
        $this->payPalAddress = $payPalAddress;
    
        return $this;
    }

    /**
     * Get payPalAddress
     *
     * @return string 
     */
    public function getPayPalAddress()
    {
        return $this->payPalAddress;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Trainer
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Trainer
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set bio
     *
     * @param string $bio
     * @return Trainer
     */
    public function setBio($bio)
    {
        $this->bio = $bio;
    
        return $this;
    }

    /**
     * Get bio
     *
     * @return string 
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Set sessionEnvironments
     *
     * @param array $sessionEnvironments
     * @return Trainer
     */
    public function setSessionEnvironments($sessionEnvironments)
    {
        $this->sessionEnvironments = $sessionEnvironments;
    
        return $this;
    }

    /**
     * Get sessionEnvironments
     *
     * @return array 
     */
    public function getSessionEnvironments()
    {
        return $this->sessionEnvironments;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Trainer
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    
        return $this;
    }

    /**
     * Get photo
     *
     * @return string 
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Trainer
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    
        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Trainer
     */
    public function setPhone($phone)
    {
        $this->phoneNumber = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phoneNumber;
    }

    /**
     * Set experience
     *
     * @param string $experience
     * @return Trainer
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;
    
        return $this;
    }

    /**
     * Get experience
     *
     * @return string 
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set languages
     *
     * @param array $languages
     * @return Trainer
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
    
        return $this;
    }

    /**
     * Get languages
     *
     * @return array 
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Trainer
     */
    public function setLocation($location)
    {
        $this->location = $location;
    
        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set specialisms
     *
     * @param array $specialisms
     * @return Trainer
     */
    public function setSpecialisms($specialisms)
    {
        $this->specialisms = $specialisms;
    
        return $this;
    }

    /**
     * Get specialisms
     *
     * @return array 
     */
    public function getSpecialisms()
    {
        return $this->specialisms;
    }

    /**
     * Set qualifications
     *
     * @param array $qualifications
     * @return Trainer
     */
    public function setQualifications($qualifications)
    {
        $this->qualifications = $qualifications;
    
        return $this;
    }

    /**
     * Get qualifications
     *
     * @return array 
     */
    public function getQualifications()
    {
        return $this->qualifications;
    }

    /**
     * Set billingCurrency
     *
     * @param string $billingCurrency
     * @return Trainer
     */
    public function setBillingCurrency($billingCurrency)
    {
        $this->billingCurrency = $billingCurrency;
    
        return $this;
    }

    /**
     * Get billingCurrency
     *
     * @return string 
     */
    public function getBillingCurrency()
    {
        return $this->billingCurrency;
    }

    /**
     * Set repsVerified
     *
     * @param boolean $repsVerified
     * @return Trainer
     */
    public function setRepsVerified($repsVerified)
    {
        $this->repsVerified = $repsVerified;
    
        return $this;
    }

    /**
     * Get repsVerified
     *
     * @return boolean 
     */
    public function getRepsVerified()
    {
        return $this->repsVerified;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Trainer
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    
        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return Trainer
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    
        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string 
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Trainer
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set lastPaidAt
     *
     * @param \DateTime $lastPaidAt
     * @return Trainer
     */
    public function setLastPaidAt($lastPaidAt)
    {
        $this->lastPaidAt = $lastPaidAt;
    
        return $this;
    }

    /**
     * Get lastPaidAt
     *
     * @return \DateTime 
     */
    public function getLastPaidAt()
    {
        return $this->lastPaidAt;
    }

    /**
     * Set lastPaidAmount
     *
     * @param string $lastPaidAmount
     * @return Trainer
     */
    public function setLastPaidAmount($lastPaidAmount)
    {
        $this->lastPaidAmount = $lastPaidAmount;
    
        return $this;
    }

    /**
     * Get lastPaidAmount
     *
     * @return string 
     */
    public function getLastPaidAmount()
    {
        return $this->lastPaidAmount;
    }

    /**
     * Set expireDate
     *
     * @param \DateTime $expireDate
     * @return Trainer
     */
    public function setExpireDate($expireDate)
    {
        $this->expireDate = $expireDate;
    
        return $this;
    }

    /**
     * Get expireDate
     *
     * @return \DateTime 
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }

    /**
     * Add clients
     *
     * @param \FitFix\CoreBundle\Entity\Client $clients
     * @return Trainer
     */
    public function addClient(\FitFix\CoreBundle\Entity\Client $clients)
    {
        $this->clients[] = $clients;
    
        return $this;
    }

    /**
     * Remove clients
     *
     * @param \FitFix\CoreBundle\Entity\Client $clients
     */
    public function removeClient(\FitFix\CoreBundle\Entity\Client $clients)
    {
        $this->clients->removeElement($clients);
    }

    /**
     * Get clients
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * Add mealplans
     *
     * @param \FitFix\CoreBundle\Entity\Mealplan $mealplans
     * @return Trainer
     */
    public function addMealplan(\FitFix\CoreBundle\Entity\Mealplan $mealplans)
    {
        $this->mealplans[] = $mealplans;
    
        return $this;
    }

    /**
     * Remove mealplans
     *
     * @param \FitFix\CoreBundle\Entity\Mealplan $mealplans
     */
    public function removeMealplan(\FitFix\CoreBundle\Entity\Mealplan $mealplans)
    {
        $this->mealplans->removeElement($mealplans);
    }

    /**
     * Get mealplans
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMealplans()
    {
        return $this->mealplans;
    }

    /**
     * Add packages
     *
     * @param \FitFix\CoreBundle\Entity\Package $packages
     * @return Trainer
     */
    public function addPackage(\FitFix\CoreBundle\Entity\Package $packages)
    {
        $this->packages[] = $packages;
    
        return $this;
    }

    /**
     * Remove packages
     *
     * @param \FitFix\CoreBundle\Entity\Package $packages
     */
    public function removePackage(\FitFix\CoreBundle\Entity\Package $packages)
    {
        $this->packages->removeElement($packages);
    }

    /**
     * Get packages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPackages()
    {
        return $this->packages;
    }

    /**
     * Set user
     *
     * @param \FitFix\CoreBundle\Entity\User $user
     * @return Trainer
     */
    public function setUser(\FitFix\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \FitFix\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add workouts
     *
     * @param \FitFix\CoreBundle\Entity\Workout $workouts
     * @return Trainer
     */
    public function addWorkout(\FitFix\CoreBundle\Entity\Workout $workouts)
    {
        $this->workouts[] = $workouts;
    
        return $this;
    }

    /**
     * Remove workouts
     *
     * @param \FitFix\CoreBundle\Entity\Workout $workouts
     */
    public function removeWorkout(\FitFix\CoreBundle\Entity\Workout $workouts)
    {
        $this->workouts->removeElement($workouts);
    }

    /**
     * Get workouts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWorkouts()
    {
        return $this->workouts;
    }

    /**
     * Add invoices
     *
     * @param \FitFix\CoreBundle\Entity\Invoice $invoices
     * @return Trainer
     */
    public function addInvoice(\FitFix\CoreBundle\Entity\Invoice $invoices)
    {
        $this->invoices[] = $invoices;
    
        return $this;
    }

    /**
     * Remove invoices
     *
     * @param \FitFix\CoreBundle\Entity\Invoice $invoices
     */
    public function removeInvoice(\FitFix\CoreBundle\Entity\Invoice $invoices)
    {
        $this->invoices->removeElement($invoices);
    }

    /**
     * Get invoices
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * Add locations
     *
     * @param \FitFix\CoreBundle\Entity\Address $locations
     * @return Trainer
     */
    public function addLocation(\FitFix\CoreBundle\Entity\Address $locations)
    {
        $this->locations[] = $locations;
    
        return $this;
    }

    /**
     * Remove locations
     *
     * @param \FitFix\CoreBundle\Entity\Address $locations
     */
    public function removeLocation(\FitFix\CoreBundle\Entity\Address $locations)
    {
        $this->locations->removeElement($locations);
    }

    /**
     * Get locations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Add reviews
     *
     * @param \FitFix\CoreBundle\Entity\Review $reviews
     * @return Trainer
     */
    public function addReview(\FitFix\CoreBundle\Entity\Review $reviews)
    {
        $this->reviews[] = $reviews;
    
        return $this;
    }

    /**
     * Remove reviews
     *
     * @param \FitFix\CoreBundle\Entity\Review $reviews
     */
    public function removeReview(\FitFix\CoreBundle\Entity\Review $reviews)
    {
        $this->reviews->removeElement($reviews);
    }

    /**
     * Get reviews
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReviews()
    {
        return $this->reviews;
    }
    
    public function isExpired(){
    	return false;
    }

    /**
     * Set terms
     *
     * @param string $terms
     * @return Trainer
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;
    
        return $this;
    }

    /**
     * Get terms
     *
     * @return string 
     */
    public function getTerms()
    {
        return $this->terms;
    }
    
    /**
     * @VirtualProperty
     * @SerializedName("email")
     *
     * @Groups({"trainer-profile"})
     */
    public function getEmail(){
    	return $this->user->getEmail();
    }
    
   /**
    * @VirtualProperty
    * @SerializedName("numberOfClients")
    *
    * @Groups({"trainer-profile"})
    */
    public function getNumberOfClients(){
    	return $this->clients->count();
    }
	
	/**
	 *
	 * @return the unknown
	 */
	public function getAddressNumber() {
		return $this->addressNumber;
	}
	
	/**
	 *
	 * @param
	 *        	$addressNumber
	 */
	public function setAddressNumber($addressNumber) {
		$this->addressNumber = $addressNumber;
		return $this;
	}
	
	/**
	 *
	 * @return the unknown
	 */
	public function getAddressStreet() {
		return $this->addressStreet;
	}
	
	/**
	 *
	 * @param
	 *        	$addressStreet
	 */
	public function setAddressStreet($addressStreet) {
		$this->addressStreet = $addressStreet;
		return $this;
	}
	
	/**
	 *
	 * @return the unknown
	 */
	public function getAddressTown() {
		return $this->addressTown;
	}
	
	/**
	 *
	 * @param
	 *        	$addressTown
	 */
	public function setAddressTown($addressTown) {
		$this->addressTown = $addressTown;
		return $this;
	}
	
	/**
	 *
	 * @return the unknown
	 */
	public function getAddressPostCode() {
		return $this->addressPostCode;
	}
	
	/**
	 *
	 * @param
	 *        	$addressPostCode
	 */
	public function setAddressPostCode($addressPostCode) {
		$this->addressPostCode = $addressPostCode;
		return $this;
	}
	
	/**
	 *
	 * @return the unknown
	 */
	public function getAddressCountry() {
		return $this->addressCountry;
	}
	
	/**
	 *
	 * @param
	 *        	$addressCountry
	 */
	public function setAddressCountry($addressCountry) {
		
		if($addressCountry == 'UK'){
			$this->billingCurrency = utf8_encode('GBP:�');
		} else if($addressCountry == 'USA') {
			$this->billingCurrency = utf8_encode('USD:$');
		} else if($addressCountry == 'Australia') {
			$this->billingCurrency = utf8_encode('AUS:$');
		} else {
			$this->billingCurrency = utf8_encode('EUR:�');
		}
		
		$this->addressCountry = $addressCountry;
		return $this;
	}
	public function getUuid() {
		return $this->uuid;
	}
	public function setUuid($uuid) {
		$this->uuid = $uuid;
		return $this;
	}
    public function getTrial() {
        return $this->uuid;
    }
    public function setTrial($uuid) {
        $this->uuid = $uuid;
        return $this;
    }
	
	
}