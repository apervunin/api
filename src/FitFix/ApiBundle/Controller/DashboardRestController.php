<?php

namespace FitFix\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\SecurityExtraBundle\Annotation\Secure;
use FitFix\CoreBundle\Model\PTDashboard;
use FitFix\CoreBundle\Model\ClientDashboard;
use JMS\Serializer\SerializationContext;
use FitFix\CoreBundle\Model\BaseDashboard;
use FitFix\CoreBundle\Entity\Notification;
use FitFix\CoreBundle\Entity\WorkoutLog;
use FitFix\CoreBundle\Entity\Session;
use FitFix\CoreBundle\Entity\User;
use FitFix\CoreBundle\Entity\Invoice;

class DashboardRestController extends FOSRestController {
	
	/**
	 * @ApiDoc(
	 * section="Dashboard"
	 * )
	 *
	 * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
	 */
	public function getDashboardAction() {
		$user = $this->getUser ();
		
		$group = ($this->getUser ()->getTrainer ()) ? "PT" : "client";
		
		$dashboardClass = "FitFix\CoreBundle\Model\\" . ucfirst ( $group ) . "Dashboard";
		
		$dashboardModel = new $dashboardClass ();
		
		$this->populateDashboardModel ( $dashboardModel, $user, $group );
		
		$view = $this->view ( $dashboardModel );
		
		$context = SerializationContext::create ()->setGroups ( array (
				'dashboard-' . strtolower ( $group ),
				'event-details',
				'address-details'
		) );
		$context->setSerializeNull ( true );
		$view->setSerializationContext ( $context );
		
		return $this->handleView($view);
	}
	
	private function populateDashboardModel(BaseDashboard $dashboard, $user, $group) {
		$dashboard->setNotifications ( $this->getNotifications ( $user, 10 ) );
		$dashboard->setNextAppointment ( $this->getNextSession ( $user ) );
		
		$populationClass = 'populateDashboardModelFor' . ucfirst ( $group );
		$this->$populationClass ( $dashboard, $user );
	}
	
	private function populateDashboardModelForPT(PTDashboard $dashboard, User $user) {
		$em = $this->getDoctrine ()->getManager ();
		
		$dashboard->setIncome ( array (
				'week' => ( float ) $em->getRepository ( 'FitFixCoreBundle:Invoice' )->getTotalIncomeForPTForPeriod ( $user, 'WEEK' ),
				'month' => ( float ) $em->getRepository ( 'FitFixCoreBundle:Invoice' )->getTotalIncomeForPTForPeriod ( $user, 'MONTH' ),
				'year' => ( float ) $em->getRepository ( 'FitFixCoreBundle:Invoice' )->getTotalIncomeForPTForPeriod ( $user, 'YEAR' ) 
		) );
		
		$dashboard->setIncomeGrowth ( array (
				'week' => ( float ) $em->getRepository ( 'FitFixCoreBundle:Invoice' )->getIncomeGrowthForPTForPeriod ( $user, 'WEEK' ),
				'month' => ( float ) $em->getRepository ( 'FitFixCoreBundle:Invoice' )->getIncomeGrowthForPTForPeriod ( $user, 'MONTH' ),
				'year' => ( float ) $em->getRepository ( 'FitFixCoreBundle:Invoice' )->getIncomeGrowthForPTForPeriod ( $user, 'YEAR' ) 
		) );
		
		$dashboard->setSessions ( $em->getRepository ( 'FitFixCoreBundle:Session' )->getTotalSessionsByMonthForPT ( $user ) );
		
		$dashboard->setTrainer($user->getTrainer());
	}
	private function populateDashboardModelForClient(ClientDashboard $dashboard, User $user) {
		$lastWorkoutLog = $this->getLastWorkoutLog ( $user );
		
		if ($lastWorkoutLog) {
			$dashboard->setLastWorkoutLog ( $lastWorkoutLog );
		}
		
		$sessionsRepo = $this->getDoctrine ()->getManager ()->getRepository ( 'FitFixCoreBundle:Session' );
		$sessionCount = $sessionsRepo->getTotalSessionsForClient ( $user );
		$sessionsBooked = $sessionsRepo->getTotalBookedSessionsForClient ( $user );
		
		$dashboard->setTotalSessions ( $sessionCount );
		$dashboard->setSessionsBooked ( $sessionsBooked );
		
		$dashboard->setGoals ( $user->getClient ()->getGoals () );
		$dashboard->setSpecificAim ( $user->getClient ()->getSpecificAim () );
		
		$dashboard->setClient($user->getClient());
		
		$contactInformation = array (
				'email' => $user->getEmail (),
				'mobile' => $user->getClient ()->getMobile (),
				'address' => $user->getClient ()->getAddress (),
				'dob' => $user->getClient ()->getDob (),
				'gender' => $user->getClient ()->getGender (),
				'clientSince' => $user->getRegistered (),
				'trial' => $user->getClient ()->getGender ()->getTrial()
		);
		
		$dashboard->setContactInformation ( $contactInformation );
	}
	private function getLastWorkoutLog($user) {
		$repo = $this->getDoctrine ()->getManager ()->getRepository ( 'FitFixCoreBundle:WorkoutLog' );
		
		$lastWorkout = $repo->findOneBy ( array (), array (
				'dateLogged' => 'DESC' 
		) );
		
		return $lastWorkout;
	}
	private function getNextSession($user) {
		$nextSession = $this->getDoctrine ()->getManager ()->getRepository ( 'FitFixCoreBundle:Session' )->findNextSessionForUser ( $user );
		return $nextSession;
	}
	private function getNotifications($user, $limit = 10) {
		$results = array ();
		
		$repo = $this->getDoctrine ()->getManager ()->getRepository ( 'FitFixCoreBundle:Notification' );
		
		$results = $repo->findBy ( array (
				'user' => $user 
		), null, $limit );
		
		return $results;
	}
}
