<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Rhumsaa\Uuid\Uuid;

/**
 * Lifestyle
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\LifestyleRepository")
 */
class Lifestyle
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var UUID
     *
     * @ORM\Column(type="string", length=255)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="bunged", type="string", length=255)
     */
    private $bunged;

    /**
     * @var string
     *
     * @ORM\Column(name="sleepquality", type="text")
     */
    private $sleepquality;

    /**
     * @var string
     *
     * @ORM\Column(name="sleeptime", type="text")
     */
    private $sleeptime;
    
    /*
     * @var string
     * 
     * @ORM\Column(name="smoke", type="text")
     */
    private $smoke;

    /**
     * @var string
     *
     * @ORM\Column(name="temper", type="text")
     */
    private $temper;

    /**
     * @var string
     *
     * @ORM\Column(name="tiredness", type="text")
     */
    private $tiredness;

    /**
     * @var string
     *
     * @ORM\Column(name="workhours", type="text")
     */
    private $workhours;

    /**
     * @var string
     *
     * @ORM\Column(name="worktravel", type="text")
     */
    private $worktravel;

    /**
     * @var string
     *
     * @ORM\Column(name="worktraveltime", type="text")
     */
    private $worktraveltime;

    /**
     * @var string
     *
     * @ORM\Column(name="worry", type="text")
     */
    private $worry;


    public function __construct(){
    	$this->uuid = Uuid::uuid4() . '-' . Uuid::uuid4();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bunged
     *
     * @param string $bunged
     * @return Lifestyle
     */
    public function setBunged($bunged)
    {
        $this->bunged = $bunged;
    
        return $this;
    }

    /**
     * Get bunged
     *
     * @return string 
     */
    public function getBunged()
    {
        return $this->bunged;
    }

    /**
     * Set sleepquality
     *
     * @param string $sleepquality
     * @return Lifestyle
     */
    public function setSleepquality($sleepquality)
    {
        $this->sleepquality = $sleepquality;
    
        return $this;
    }

    /**
     * Get sleepquality
     *
     * @return string 
     */
    public function getSleepquality()
    {
        return $this->sleepquality;
    }

    /**
     * Set sleeptime
     *
     * @param string $sleeptime
     * @return Lifestyle
     */
    public function setSleeptime($sleeptime)
    {
        $this->sleeptime = $sleeptime;
    
        return $this;
    }

    /**
     * Get sleeptime
     *
     * @return string 
     */
    public function getSleeptime()
    {
        return $this->sleeptime;
    }


	/**
	 * @return the unknown_type
	 */
	public function getSmoke() {
		return $this->smoke;
	}
	
	/**
	 * @param unknown_type $smoke
	 */
	public function setSmoke($smoke) {
		$this->smoke = $smoke;
		return $this;
	}
	
    /**
     * Set temper
     *
     * @param string $temper
     * @return Lifestyle
     */
    public function setTemper($temper)
    {
        $this->temper = $temper;
    
        return $this;
    }

    /**
     * Get temper
     *
     * @return string 
     */
    public function getTemper()
    {
        return $this->temper;
    }

    /**
     * Set tiredness
     *
     * @param string $tiredness
     * @return Lifestyle
     */
    public function setTiredness($tiredness)
    {
        $this->tiredness = $tiredness;
    
        return $this;
    }

    /**
     * Get tiredness
     *
     * @return string 
     */
    public function getTiredness()
    {
        return $this->tiredness;
    }

    /**
     * Set workhours
     *
     * @param string $workhours
     * @return Lifestyle
     */
    public function setWorkhours($workhours)
    {
        $this->workhours = $workhours;
    
        return $this;
    }

    /**
     * Get workhours
     *
     * @return string 
     */
    public function getWorkhours()
    {
        return $this->workhours;
    }

    /**
     * Set worktravel
     *
     * @param string $worktravel
     * @return Lifestyle
     */
    public function setWorktravel($worktravel)
    {
        $this->worktravel = $worktravel;
    
        return $this;
    }

    /**
     * Get worktravel
     *
     * @return string 
     */
    public function getWorktravel()
    {
        return $this->worktravel;
    }

    /**
     * Set worktraveltime
     *
     * @param string $worktraveltime
     * @return Lifestyle
     */
    public function setWorktraveltime($worktraveltime)
    {
        $this->worktraveltime = $worktraveltime;
    
        return $this;
    }

    /**
     * Get worktraveltime
     *
     * @return string 
     */
    public function getWorktraveltime()
    {
        return $this->worktraveltime;
    }

    /**
     * Set worry
     *
     * @param string $worry
     * @return Lifestyle
     */
    public function setWorry($worry)
    {
        $this->worry = $worry;
    
        return $this;
    }

    /**
     * Get worry
     *
     * @return string 
     */
    public function getWorry()
    {
        return $this->worry;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Lifestyle
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    
        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }
}