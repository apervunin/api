<?php

namespace FitFix\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', array(
        	'attr' => array(
            	'class' => 'text',
        		'placeholder' => 'Enter your email address'
            )
        ))
            ->add('plainPassword', 'text', array(
            		'attr' => array(
            				'class' => 'text',
            				'placeholder' => 'Password'
            		),
                'required' => false,
                'label' => 'Plain Password'
            ))

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FitFix\CoreBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'fitfix_adminbundle_usertype';
    }
}
