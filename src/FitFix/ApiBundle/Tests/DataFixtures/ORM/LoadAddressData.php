<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Address;
use FitFix\CoreBundle\Entity\SessionType;
use FitFix\CoreBundle\Entity\SessionTypeLocation;
use FitFix\CoreBundle\Entity\SessionTypeLocationDate;
use FitFix\CoreBundle\Entity\SessionTypeLocationDateTime;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadAddressData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // address-1
        $address = new Address();
        $address->setName('Gym');
        $address->setAddress1('1 High Street');
        $address->setTown('Tunbridge Wells');
        $address->setCounty('Kent');
        $address->setPostcode('TN1 1AB');
        $address->setCountry('GB');
        $address->setLongitude(51.129195);
        $address->setLatitude(0.262878);

        $this->addReference('address-1', $address);
        $manager->persist($address);

        // address-2
        $address = new Address();
        $address->setName('Sports ground');
        $address->setAddress1('1 Hawkenbury Road');
        $address->setTown('Tunbridge Wells');
        $address->setCounty('Kent');
        $address->setPostcode('TN2 1CD');
        $address->setCountry('GB');
        $address->setLongitude(51.129195);
        $address->setLatitude(0.262878);

        $this->addReference('address-2', $address);
        $manager->persist($address);

        // address-3
        $address = new Address();
        $address->setName('Fusion Sports Centre');
        $address->setAddress1('100 London Road');
        $address->setTown('Tunbridge Wells');
        $address->setCounty('Kent');
        $address->setPostcode('TN3 1EF');
        $address->setCountry('GB');
        $address->setLongitude(51.129195);
        $address->setLatitude(0.262878);

        $this->addReference('address-3', $address);
        $manager->persist($address);

        $manager->flush();

    }

    public function getOrder()
    {
        return 8; // the order in which fixtures will be loaded
    }

}
