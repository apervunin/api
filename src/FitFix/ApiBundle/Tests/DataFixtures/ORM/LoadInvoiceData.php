<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Invoice;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadInvoiceData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // invoice-1
        $invoice = new Invoice();
        $invoice->setPaid(new DateTime('2013-08-10 10:00:00'));
        $invoice->setPrice(20);
        $invoice->setStatus('unpaid');
        $invoice->setFrom($this->getReference('trainer-1'));
        $invoice->setTo($this->getReference('client-1'));
        $invoice->setDescription('Lorem ipsum');

        $this->addReference('invoice-1', $invoice);
        $manager->persist($invoice);

        // invoice-2
        $invoice = new Invoice();
        $invoice->setPaid(new DateTime('2013-08-10 10:00:00'));
        $invoice->setPrice(30);
        $invoice->setStatus('unpaid');
        $invoice->setFrom($this->getReference('trainer-2'));
        $invoice->setTo($this->getReference('client-2'));
        $invoice->setDescription('Lorem ipsum');

        $this->addReference('invoice-2', $invoice);
        $manager->persist($invoice);

        $manager->flush();

    }

    public function getOrder()
    {
        return 14; // the order in which fixtures will be loaded
    }

}
