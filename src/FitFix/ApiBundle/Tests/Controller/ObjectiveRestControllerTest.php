<?php

namespace FitFix\ApiBundle\Tests\Controller;

use FitFix\CoreBundle\Entity\Client;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use Symfony\Component\BrowserKit\Client as BrowserClient;

/**
 * Test class for objective rest controller
 *
 */
class ObjectiveRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var BrowserClient
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var EntityManager
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {
        $this->client = static::createClient();
        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');

        $this->em = static::$kernel->getContainer() ->get('doctrine') ->getEntityManager();

        /*
         * Creation of the browser client with the client1 authenticated header
         */
        $user = $this->userManager->findUserByUsername("client1");
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );
            $this->client->setServerParameters($this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get objectives
     *
     * @return none
     */
    public function testGetObjectivesAction_valid_user()
    {
        $this->client->request('GET', '/api/clients/client1/objectives');
        $content = $this->client->getResponse()->getContent();
        $objectives = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(1, $objectives);
    }

    /**
     * Test get objective
     *
     * @return none
     */
    public function testGetObjectiveAction_valid_user()
    {
        $this->client->request('GET', '/api/clients/client1/objectives/1');
        $content = $this->client->getResponse()->getContent();
        $objective = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertEquals('Get Fit', $objective->description);
    }

    /**
     * Test get client nonexistent slug
     *
     * @return none
     */
    public function testGetObjectiveAction_invalid_client()
    {
        $this->client->request('GET', '/api/clients/client0/objectives');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get client unauthorised slug
     *
     * @return none
     */
    public function testGetObjectiveAction_unauthorised_client()
    {
        $this->client->request('GET', '/api/clients/client2/objectives');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test create objective
     *
     * @return none
     */
    public function testPostObjectivesAction()
    {

        $params = array(
            'description'      => 'Eat Well'
        );

        $this->client->request('POST', '/api/clients/client1/objectives', $params);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $this->assertRegExp('/\/api\/clients\/client1\/objectives\/3/', $this->client->getResponse()->headers->get('location'));

        $objective = $this->em->getRepository('FitFixCoreBundle:Objective')->find(3);
        $this->assertNotNull($objective, "Objective created");
        if ($objective) {
            $this->em->remove($objective);
            $this->em->flush();
        }
    }

    /**
     * Test create objective invalid data
     *
     * @return none
     */
    public function testPostObjectivesAction_invalid_data()
    {
        $params = array(
            'description'      => ''
        );

        $this->client->request('POST', '/api/clients/client1/objectives', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"description":["Please enter a description"]}';
        $this->assertEquals($expected, $this->client->getResponse()->getContent());

    }

    /**
     * Test delete an existing objective
     *
     * @return none
     */
    public function testDeleteObjectiveAction()
    {
        $this->client->request('DELETE', '/api/clients/client1/objectives/2');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete objective nonexistent
     *
     * @return none
     */
    public function testDeleteObjectiveAction_invalid_objective()
    {
        $this->client->request('DELETE', '/api/clients/client1/objectives/0');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete objective without id
     *
     * @return none
     */
    public function testDeleteObjectiveAction_without_id()
    {
        $this->client->request('DELETE', '/api/clients/client1/objectives');

        $this->assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update objective
     *
     * @return none
     */
    public function testPutObjectiveAction()
    {
        $params = array('description' => 'Eat fresh fruit');

        $this->client->request('PUT', '/api/clients/client1/objectives/1', $params);

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }

    /**
     * Test update objective invalid id
     *
     * @return none
     */
    public function testPutObjectiveAction_invalid_id()
    {
        $params = array('description' => 'Eat fresh fruit');

        $this->client->request('PUT', '/api/clients/client1/objectives/0', $params);

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update objective unauthorised slug
     *
     * @return none
     */
    public function testPutObjectiveAction_unauthorised_client()
    {
        $params = array('description' => 'Eat fresh fruit');

        $this->client->request('GET', '/api/clients/client2/objectives/1', $params);

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get options
     *
     * @return none
     */
    public function testOptionsObjectivesAction()
    {
        $this->client->request('OPTIONS', '/api/client/objectives', array(), array(), $this->header);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('access-control-allow-methods', 'OPTIONS, GET, POST, PUT, DELETE'));
    }
}
