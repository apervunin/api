define([
    'jquery',
    'lodash',
    'backbone',
    'vm',
    'events',
    'models/session',
    'text!templates/layout.html'
], function($, _, Backbone, Vm, Events, Session, layoutTemplate){
    var AppView = Backbone.View.extend({
        el: '.container',
        initialize: function () {

            $.ajaxPrefilter( function( options, originalOptions, jqXHR ) {
                // Your server goes below
                //options.url = 'http://localhost:8000' + options.url;
                options.url = '/app_dev.php' + options.url;
            });



        },
        render: function () {
            var that = this;
            $(this.el).html(layoutTemplate);
            require(['views/header/menu'], function (HeaderMenuView) {
                var headerMenuView = Vm.create(that, 'HeaderMenuView', HeaderMenuView);
                headerMenuView.render();
            });
            require(['views/footer/footer'], function (FooterView) {
                // Pass the appView down into the footer so we can render the visualisation
                var footerView = Vm.create(that, 'FooterView', FooterView, {appView: that});
                footerView.render();
            });

        },
        events: {
          'click .logout': 'logout'
        },
        logout: function (ev) {

          Session.logout();
          // Disable the button
          //$(ev.currentTarget).text('Logging out').attr('disabled', 'disabled');
          ev.preventDefault();
        }
    });
    return AppView;
});
