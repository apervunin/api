<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

use Gedmo\Mapping\Annotation as gedmo;
use Gedmo\Timestampable\Timestampable;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\SerializedName;
use Rhumsaa\Uuid\Uuid;

/**
 * Invoice
 *
 * @ORM\Table(name="invoice")
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\InvoiceRepository")
 * @ExclusionPolicy("all")
 */
class Invoice
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"invoice-list", "invoice-details", "event-list", "event-details", "packagepurchase-details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="datetime", nullable=true, name="paid")
     * @Assert\DateTime(
     *     message="Please enter a valid paid date/time (YYYY-MM-DD HH:MM:SS)"
     * )
     * @Expose
     * @Groups({"invoice-list", "invoice-details", "event-details", "packagepurchase-details"})
     */
    private $paid;

    /**
     * @var decimal $price
     *
     * @ORM\Column(type="decimal", nullable=false, name="price", scale=2)
     * @Assert\NotBlank(
     *     message="Please enter a price"
     * )
     * @Assert\Regex(
     *     pattern="/^\d+(\.\d{1,2})?$/",
     *     message="You must enter a number"
     * )
     * @Expose
     * @Groups({"invoice-list", "invoice-details",  "event-details", "packagepurchase-details"})
     */
    private $price;

    /**
     * @var string $status
     *
     * @ORM\Column(type="string", nullable=false, name="status")
     * @Assert\Choice(choices = {"paid", "failed", "unpaid", "sent"}, message = "Please choose a valid status")
     * @Expose
     * @Groups({"invoice-list", "invoice-details", "event-list",  "event-details", "packagepurchase-details"})
     */
    private $status;

    /**
     * @var string paymentMethod
     * @ORM\Column(type="string", nullable=true, name="paymentMethod")
     * @Assert\Choice(choices = {"BACS", "Cash", "PayPal"}, message = "Please choose a valid payment method")
     * @Expose
     * @Groups({"invoice-list", "invoice-details", "event-details", "event-list", "event-details"})
     * @SerializedName("paymentMethod") 
     */
    private $paymentMethod;
    
    /**
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Trainer", inversedBy="invoices")
     * @Assert\NotBlank(
     *     message="Please select a trainer"
     * )
     * @Expose
     * @Groups({"invoice-list", "invoice-details"})
     */
    private $from;

    /**
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Client", inversedBy="invoices")
     * @Assert\NotBlank(
     *     message="Please select a client"
     * )
     * @Expose
     * @Groups({"invoice-list", "invoice-details"})
     */
    private $to;


    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", nullable=false, name="created_at")
     * @gedmo\Timestampable(on="create")
     * @Expose
     * @Groups({"invoice-list", "invoice-details", "packagepurchase-details"})
     * @SerializedName("createdAt")
     */
    protected $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", nullable=false, name="updated_at")
     * @gedmo\Timestampable(on="update")
     * @Expose
     * @Groups({"invoice-details"})
     * @SerializedName("updatedAt")
     */
    protected $updatedAt;
    
    /**
     * @var string
     * 
     * @ORM\Column(type="string", nullable=false, name="description")
     * @Expose
     * @Groups({"invoice-details"})
     */
    protected $description;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="tag")
     * @Expose
     * @Groups({"invoice-details", "invoice-list"})
     */
    protected $tag;
    
    /**
     * @var UUID
     * 
     * @ORM\Column(type="string")
     * 
     * @Expose
     * @Groups({"invoice-details", "invoice-list"})
     */
    private $uuid;

    public function __construct() {
    	$this->uuid = Uuid::uuid4();
    	$this->setCreatedAt(new \DateTime());
    	$this->setUpdatedAt(new \DateTime());  	
    	$this->setStatus("unpaid");
    }
    
   	public function __toString() {
   		return (string) $this->getId();
   	}
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paid
     *
     * @param \DateTime $paid
     * @return Invoice
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return \DateTime
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Invoice
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Invoice
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set trainer
     *
     * @param \FitFix\CoreBundle\Entity\Trainer $trainer
     * @return Invoice
     */
    public function setFrom(\FitFix\CoreBundle\Entity\Trainer $trainer = null)
    {
        $this->from = $trainer;

        return $this;
    }

    /**
     * Get trainer
     *
     * @return \FitFix\CoreBundle\Entity\Trainer
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set client
     *
     * @param \FitFix\CoreBundle\Entity\Client $client
     * @return Invoice
     */
    public function setTo(\FitFix\CoreBundle\Entity\Client $client = null)
    {
        $this->to = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \FitFix\CoreBundle\Entity\Client
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Invoice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Invoice
     */
    public function setStatus($status)
    {
        $this->status = $status;
        
        if($this->status == 'unpaid'){
        	$this->paymentMethod = "";
        }

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

	/**
	 * @return the string
	 */
	public function getTag() {
		return $this->tag;
	}
	
	/**
	 * @param string $tag
	 */
	public function setTag($tag) {
		$this->tag = $tag;
		return $this;
	}

	/**
	 * @return the string
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
		return $this;
	}
	
	/**
	 *
	 * @return the string
	 */
	public function getPaymentMethod() {
		return $this->paymentMethod;
	}
	
	/**
	 *
	 * @param string $paymentMethod        	
	 */
	public function setPaymentMethod($paymentMethod) {
		$this->paymentMethod = $paymentMethod;
		return $this;
	}
	
	
	

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Invoice
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    
        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }
}