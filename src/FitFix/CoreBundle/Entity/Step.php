<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * Step
 *
 * @ORM\Table(name="step")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Step
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     * @Groups({"workout-list", "workout-details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false, name="name")
     * @Expose
     * @Groups({"workout-list", "workout-details", "workoutLog-details"})
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false, name="reps")
     * @Expose
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $reps;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false, name="weight")
     * @Expose
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $weight;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false, name="sets")
     * @Expose
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $sets;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false, name="tempo")
     * @Expose
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $tempo;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false, name="rest")
     * @Expose
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $rest;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Exercise", cascade={"persist"})
     * @ORM\JoinColumn(name="exercise_id", referencedColumnName="id")
     * @Expose
     * @Groups({"workout-list", "workout-details", "workoutLog-details"})
     */
    private $exercise;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Technique", cascade={"persist"})
     * @ORM\JoinColumn(name="technique_id", referencedColumnName="id")
     * @Expose
     * @Groups({"workout-list", "workout-details", "workoutLog-details"})
     */
    private $technique;
    
    /**
     * @var Workout
     * 
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Workout", inversedBy="workout", cascade={"persist"})
     * @ORM\JoinColumn(name="workout_id", referencedColumnName="id")
     * @Expose
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $workout;
    
    /**
     * @var Warmdown
     * 
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Workout", inversedBy="warmdown", cascade={"persist"})
     * @ORM\JoinColumn(name="warmdown_id", referencedColumnName="id")
     * @Expose
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $warmdown;
    
    /**
     * @var Warmup
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Workout", inversedBy="warmup", cascade={"persist"})
     * @ORM\JoinColumn(name="warmup_id", referencedColumnName="id")
     * @Expose
     * @Groups({"workout-details", "workoutLog-details"})
     */
    private $warmup;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Step
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set reps
     *
     * @param integer $reps
     * @return Step
     */
    public function setReps($reps)
    {
        $this->reps = $reps;

        return $this;
    }

    /**
     * Get reps
     *
     * @return integer
     */
    public function getReps()
    {
        return $this->reps;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     * @return Step
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set sets
     *
     * @param integer $sets
     * @return Step
     */
    public function setSets($sets)
    {
        $this->sets = $sets;

        return $this;
    }

    /**
     * Get sets
     *
     * @return integer
     */
    public function getSets()
    {
        return $this->sets;
    }

    /**
     * Set tempo
     *
     * @param integer $tempo
     * @return Step
     */
    public function setTempo($tempo)
    {
        $this->tempo = $tempo;

        return $this;
    }

    /**
     * Get tempo
     *
     * @return integer
     */
    public function getTempo()
    {
        return $this->tempo;
    }

    /**
     * Set rest
     *
     * @param integer $rest
     * @return Step
     */
    public function setRest($rest)
    {
        $this->rest = $rest;

        return $this;
    }

    /**
     * Get rest
     *
     * @return integer
     */
    public function getRest()
    {
        return $this->rest;
    }

    /**
     * Set exercise
     *
     * @param \FitFix\CoreBundle\Entity\Exercise $exercise
     * @return Step
     */
    public function setExercise(\FitFix\CoreBundle\Entity\Exercise $exercise = null)
    {
        $this->exercise = $exercise;

        return $this;
    }

    /**
     * Get exercise
     *
     * @return \FitFix\CoreBundle\Entity\Exercise
     */
    public function getExercise()
    {
        return $this->exercise;
    }

    /**
     * Set technique
     *
     * @param \FitFix\CoreBundle\Entity\Technique $technique
     * @return Step
     */
    public function setTechnique(\FitFix\CoreBundle\Entity\Technique $technique = null)
    {
        $this->technique = $technique;

        return $this;
    }

    /**
     * Get technique
     *
     * @return \FitFix\CoreBundle\Entity\Technique
     */
    public function getTechnique()
    {
        return $this->technique;
    }

    /**
     * Set workout
     *
     * @param \FitFix\CoreBundle\Entity\Workout $workout
     * @return Step
     */
    public function setWorkout(\FitFix\CoreBundle\Entity\Workout $workout = null)
    {
        $this->workout = $workout;
    
        return $this;
    }

    /**
     * Get workout
     *
     * @return \FitFix\CoreBundle\Entity\Workout 
     */
    public function getWorkout()
    {
        return $this->workout;
    }

    /**
     * Set warmdown
     *
     * @param \FitFix\CoreBundle\Entity\Workout $warmdown
     * @return Step
     */
    public function setWarmdown(\FitFix\CoreBundle\Entity\Workout $warmdown = null)
    {
        $this->warmdown = $warmdown;
    
        return $this;
    }

    /**
     * Get warmdown
     *
     * @return \FitFix\CoreBundle\Entity\Workout 
     */
    public function getWarmdown()
    {
        return $this->warmdown;
    }

    /**
     * Set warmup
     *
     * @param \FitFix\CoreBundle\Entity\Workout $warmup
     * @return Step
     */
    public function setWarmup(\FitFix\CoreBundle\Entity\Workout $warmup = null)
    {
        $this->warmup = $warmup;
    
        return $this;
    }

    /**
     * Get warmup
     *
     * @return \FitFix\CoreBundle\Entity\Workout 
     */
    public function getWarmup()
    {
        return $this->warmup;
    }
}
