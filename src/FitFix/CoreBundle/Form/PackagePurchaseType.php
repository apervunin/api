<?php

namespace FitFix\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PackagePurchaseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('purchaseDate')
            ->add('sessions')
            ->add('invoice')
            ->add('client')
            ->add('package')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FitFix\CoreBundle\Entity\PackagePurchase'
        ));
    }

    public function getName()
    {
        return '';
    }
}
