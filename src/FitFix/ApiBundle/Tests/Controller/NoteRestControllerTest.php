<?php

namespace FitFix\ApiBundle\Tests\Controller;

use FitFix\CoreBundle\Entity\Client;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use Symfony\Component\BrowserKit\Client as BrowserClient;

/**
 * Test class for note rest controller
 *
 */
class NoteRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var BrowserClient
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var EntityManager
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {
        $this->client = static::createClient();
        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');

        $this->em = static::$kernel->getContainer() ->get('doctrine') ->getEntityManager();

        /*
         * Creation of the browser client with the client1 authenticated header
         */
        $user = $this->userManager->findUserByUsername("client1");
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );
            $this->client->setServerParameters($this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get notes
     *
     * @return none
     */
    public function testGetNotesAction_valid_user()
    {
        $this->client->request('GET', '/api/clients/client1/notes');
        $content = $this->client->getResponse()->getContent();
        $notes = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(1, $notes);
    }

    /**
     * Test get note
     *
     * @return none
     */
    public function testGetNoteAction_valid_user()
    {
        $this->client->request('GET', '/api/clients/client1/notes/1');
        $content = $this->client->getResponse()->getContent();
        $note = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertEquals('Go to Gym', $note->description);
    }

    /**
     * Test get client nonexistent slug
     *
     * @return none
     */
    public function testGetNoteAction_invalid_client()
    {
        $this->client->request('GET', '/api/clients/client0/notes');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get client unauthorised slug
     *
     * @return none
     */
    public function testGetNoteAction_unauthorised_client()
    {
        $this->client->request('GET', '/api/clients/client2/notes');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test create note
     *
     * @return none
     */
    public function testPostNotesAction()
    {

        $params = array(
            'description'      => 'Eat Well'
        );

        $this->client->request('POST', '/api/clients/client1/notes', $params);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $this->assertRegExp('/\/api\/clients\/client1\/notes\/3/', $this->client->getResponse()->headers->get('location'));

        $note = $this->em->getRepository('FitFixCoreBundle:Note')->find(3);
        $this->assertNotNull($note, "Note created");
        if ($note) {
            $this->em->remove($note);
            $this->em->flush();
        }
    }

    /**
     * Test create note invalid data
     *
     * @return none
     */
    public function testPostNotesAction_invalid_data()
    {
        $params = array(
            'description'      => ''
        );

        $this->client->request('POST', '/api/clients/client1/notes', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"description":["Please enter a description"]}';
        $this->assertEquals($expected, $this->client->getResponse()->getContent());

    }

    /**
     * Test delete an existing note
     *
     * @return none
     */
    public function testDeleteNoteAction()
    {
        $this->client->request('DELETE', '/api/clients/client1/notes/2');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete note nonexistent
     *
     * @return none
     */
    public function testDeleteNoteAction_invalid_note()
    {
        $this->client->request('DELETE', '/api/clients/client1/notes/0');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete note without id
     *
     * @return none
     */
    public function testDeleteNoteAction_without_id()
    {
        $this->client->request('DELETE', '/api/clients/client1/notes');

        $this->assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update note
     *
     * @return none
     */
    public function testPutNoteAction()
    {
        $params = array('description' => 'Eat fresh fruit');

        $this->client->request('PUT', '/api/clients/client1/notes/1', $params);

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }

    /**
     * Test update note invalid id
     *
     * @return none
     */
    public function testPutNoteAction_invalid_id()
    {
        $params = array('description' => 'Eat fresh fruit');

        $this->client->request('PUT', '/api/clients/client1/notes/0', $params);

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update note unauthorised slug
     *
     * @return none
     */
    public function testPutNoteAction_unauthorised_client()
    {
        $params = array('description' => 'Eat fresh fruit');

        $this->client->request('GET', '/api/clients/client2/notes/1', $params);

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get options
     *
     * @return none
     */
    public function testOptionsNotesAction()
    {
        $this->client->request('OPTIONS', '/api/client/notes', array(), array(), $this->header);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('access-control-allow-methods', 'OPTIONS, GET, POST, PUT, DELETE'));
    }
}
