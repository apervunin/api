<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Address;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;

use \DateTime;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller that provides Restful services over the resource Address.
 *
 * @NamePrefix("fitfix_api_addressrest_")
 * @author Patrick Lock <patrick.lock@gmail.com>
 */
class AddressRestController extends Controller
{
    /**
     * Returns allowed options.
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function optionsAddressesAction() {
        $view = FOSView::create();
        $view->setStatusCode(200);
        $view->setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
        return $view;
    }

    /**
     * Returns all addresses.
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER, ROLE_CLIENT")
     * @ApiDoc()
     */
    public function getAddressesAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        if($this->getUser()->getTrainer()){
        	$trainer = $this->getUser()->getTrainer();
        } else {
        	$trainer = $this->getUser()->getClient()->getTrainer();
        }
        
        $entities = $em->getRepository('FitFixCoreBundle:Address')->findBy(array('trainer' => $trainer, 'enabled' => true));

        $view = FOSView::create();
        
        $view->setSerializationContext(SerializationContext::create()->setGroups(array("address-list")));
        $view->setStatusCode(200)->setData($entities);
        
        return $view;
    }

    /**
     * Returns an address by id.
     *
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getAddressAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $view = FOSView::create();

        $entity = $em->getRepository('FitFixCoreBundle:Address')->findBy(array('id' => $id, 'enabled' => true));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Address entity.');
        }

        if ($entity) {
            $view->setSerializationContext(SerializationContext::create()->setGroups(array("address-details")));
            $view->setStatusCode(200)->setData($entity);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }

    /**
     * Creates a new Address entity.
     * Using param_fetcher_listener: force
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="name", requirements="\d+", default="", description="Name")
     * @RequestParam(name="address1", requirements="\d+", default="", description="Address1")
     * @RequestParam(name="address2", requirements="\d+", default="", description="Address2")
     * @RequestParam(name="town", requirements="\d+", default="", description="Town")
     * @RequestParam(name="postcode", requirements="\d+", default="", description="Postcode")
     * @RequestParam(name="country", requirements="\d+", default="", description="Country")
     * @RequestParam(name="longitude", requirements="\d+", default="", description="Longitude")
     * @RequestParam(name="latitude", requirements="\d+", default="", description="Latitude")
     * @RequestParam(name="enabled", requirements="\d+", default="", description="Whether the address is enabled/disabled")
     * @RequestParam(name="formattedAddress", requirements="\d+", default="", description="The fully formatred address of the place")
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function postAddressesAction(ParamFetcher $paramFetcher)
    {
        $view = FOSView::create();

        $request = $this->getRequest();

        $address = new Address();
        $address->setName($request->get('name'));
        $address->setAddress1($request->get('address1'));
        $address->setAddress2($request->get('address2'));
        $address->setTown($request->get('town'));
        $address->setPostcode($request->get('postcode'));
        $address->setCountry($request->get('country'));
        $address->setLongitude($request->get('longitude'));
        $address->setLatitude($request->get('latitude'));
        $address->setEnabled($request->get('enabled'));
        $address->setFormattedAddress($request->get('formattedAddress'));
        
        $trainer = $this->getUser()->getTrainer();
        $address->setTrainer($trainer);

        $validator = $this->get('validator');
        $errors = $validator->validate($address);

        if (count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($address);
            $em->flush();
           
            $view->setData($address);
        } else {
            $view = $this->get_errors_view($errors);
        }
        return $view;
    }

    /**
     * Update an address by id.
     *
     * @param string $id ID
     *
     * @RequestParam(name="name", requirements="\d+", default="", description="Name")
     * @RequestParam(name="address1", requirements="\d+", default="", description="Address1")
     * @RequestParam(name="address2", requirements="\d+", default="", description="Address2")
     * @RequestParam(name="town", requirements="\d+", default="", description="Town")
     * @RequestParam(name="postcode", requirements="\d+", default="", description="Postcode")
     * @RequestParam(name="country", requirements="\d+", default="", description="Country")
     * @RequestParam(name="longitude", requirements="\d+", default="", description="Longitude")
     * @RequestParam(name="latitude", requirements="\d+", default="", description="Latitude")
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function putAddressAction($id)
    {
        $view = FOSView::create();

        $em = $this->getDoctrine()->getManager();
        $address = $em->getRepository('FitFixCoreBundle:Address')->find($id);
        
        $trainer = $this->getUser()->getTrainer();
        
        if($address->getTrainer()->getId() !== $trainer->getId()){
        	throw new AccessDeniedHttpException();
        }

        if (!$address) {
            throw $this->createNotFoundException('Unable to find Address entity.');
        }

        $request = $this->getRequest();

        if ($request->get('name')) {
            $address->setName($request->get('name'));
        }
        if ($request->get('address1')) {
            $address->setAddress1($request->get('address1'));
        }
        if ($request->get('address2')) {
            $address->setAddress2($request->get('address2'));
        }
        if ($request->get('town')) {
            $address->setTown($request->get('town'));
        }
        if ($request->get('postcode')) {
            $address->setPostcode($request->get('postcode'));
        }
        if ($request->get('country')) {
            $address->setCountry($request->get('country'));
        }
        if ($request->get('longitude')) {
            $address->setLongitude($request->get('longitude'));
        }
        if ($request->get('latitude')) {
            $address->setLatitude($request->get('latitude'));
        }

        $validator = $this->get('validator');
        $errors = $validator->validate($address);

        if (count($errors) == 0) {
            $em->persist($address);
            $em->flush();
            $view = FOSView::create();
            $view->setStatusCode(204);
        } else {
            $view = $this->get_errors_view($errors);
        }
        return $view;
    }

    /**
     * Delete an address by ID
     *
     * @param string $id ID
     *
     * @return FOSView
     * @Secure(roles="ROLE_TRAINER")
     * @ApiDoc()
     */
    public function deleteAddressAction($id)
    {
        $view = FOSView::create();

        $em = $this->getDoctrine()->getManager();
        $address = $em->getRepository('FitFixCoreBundle:Address')->find($id);
        
        if ($address) {

        	$trainer = $this->getUser()->getTrainer();
        	
        	if($address->getTrainer()->getId() !== $trainer->getId()){
        		throw new AccessDeniedHttpException();
        	}
        	
        	$address->setEnabled(false);
            $em->persist($address);
            $em->flush();
            $view->setStatusCode(204);
        } else {
            throw $this->createNotFoundException();
        }
        return $view;
    }

    /**
     * Get the validation errors
     *
     * @param ConstraintViolationList $errors Validator error list
     *
     * @return FOSView
     */
    private function get_errors_view($errors)
    {
        $msgs = array();
        $it = $errors->getIterator();
        //$val = new \Symfony\Component\Validator\ConstraintViolation();
        foreach ($it as $val) {
            $msg = $val->getMessage();
            $params = $val->getMessageParameters();
            //using FOSUserBundle translator domain 'validators'
            $msgs[$val->getPropertyPath()][] = $this->get('translator')->trans($msg, $params, 'validators');
        }
        $view = FOSView::create($msgs);
        $view->setStatusCode(400);
        return $view;
    }

}