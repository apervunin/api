<?php

namespace FitFix\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * StaticPage controller.
 *
 * @Route("/")
 */
class StaticPageController extends Controller
{
    /**
     * Welcome page.
     *
     * @Route("/", name="frontend_welcome")
     * @Template()
     */
    public function welcomeAction()
    {
      return $this->render('FitFixFrontendBundle:StaticPage:welcome.html.twig');
    }
}
