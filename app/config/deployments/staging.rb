server 'xperia.vccp.stage.fishrod.co.uk', :app, :web, :primary => true
# server '54.228.214.226', :app

set   :deploy_to,     "/var/www/xperia"
set   :user,		  "ubuntu"
ssh_options[:keys] = [File.join(ENV["HOME"], ".ssh", "staging-environments.pem")]
ssh_options[:forward_agent] = true

set :webserver_user,      "www-data"
set :dump_assetic_assets, true