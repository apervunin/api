<?php

namespace FitFix\ApiBundle\PayloadHelper;

use FitFix\CoreBundle\Entity\Client;
use FitFix\CoreBundle\Entity\Goal;

class ClientPayloadHelper extends AbstractPayloadHelper {
	
	/**
	 * Populates the client entity from a payload
	 * 
	 * @param Client $client
	 * @param \stdClass $payload
	 */
	public static function populateEntityFromPayload($client, $payload, $prefix = 'details_'){
		
		parent::populateEntityFromPayload($client, $payload, $prefix);
		
		$client->setDob(self::generateDOB($payload->details_dobday, $payload->details_dobmonth, $payload->details_dobyear));
		
		if(!$client->getPhoto()){
			$client->setPhoto('http://fitfixappdata.s3.amazonaws.com/images/defaults/user.png');
		}
		
		$client->setGoals($payload->details_goals);
		
	}
	
	/**
	 * Generated a DOB from dat, month and year from payload
	 * @param int $day
	 * @param int $month
	 * @param int $year
	 * @return \DateTime
	 */
	public static function generateDOB($day, $month, $year){
		
		$day = (int) $day;
		$month = (int) $month;
		$year = (int) $year;
		
		$dob = new \DateTime();
		$dob->setDate($year, $month, $day);
		
		return $dob;
	}
	
}