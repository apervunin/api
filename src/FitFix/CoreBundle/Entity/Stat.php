<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Groups;

/**
 * Stat
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\StatRepository")
 * 
 * @ExclusionPolicy("all")
 */
class Stat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $id;
    
    /**
     * @var User
     * 
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\User")
     * 
     * @Groups({"stat-details"})
     * 
     * @Expose
     */
    private $user;

    /**
     * @var float
     *
     * @ORM\Column(name="height", type="float", nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $height;

    /**
     * @var string
     *
     * @ORM\Column(name="heightUnit", type="string", length=10, nullable=true)
     * 
     * @Assert\Choice(
     * 	choices={"m", "ft"}
     * )
     * 
     * @SerializedName("heightUnit")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $heightUnit;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float", nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $weight;

    /**
     * @var string
     *
     * @ORM\Column(name="weightUnit", type="string", length=10, nullable=true)
     * 
     * @Assert\Choice(
     * 	choices={"lb", "kg"}
     * )
     * 
     * @SerializedName("weightUnit")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $weightUnit;

    /**
     * @var float
     *
     * @ORM\Column(name="restingHeartRate", type="float", nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @SerializedName("restingHeartRate")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $restingHeartRate;

    /**
     * @var string
     *
     * @ORM\Column(name="neck", type="string", length=255, nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $neck;

    /**
     * @var string
     *
     * @ORM\Column(name="neckUnit", type="string", length=10, nullable=true)
     * 
     * @Assert\Choice(
     * 	choices={"cm", "in"}
     * )
     * 
     * @SerializedName("neckUnit")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $neckUnit;

    /**
     * @var float
     *
     * @ORM\Column(name="chest", type="float", nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $chest;

    /**
     * @var string
     *
     * @ORM\Column(name="chestUnit", type="string", length=10, nullable=true)
     * 
     * @Assert\Choice(
     * 	choices={"cm", "in"}
     * )
     * 
     * @SerializedName("chestUnit")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $chestUnit;

    /**
     * @var float
     *
     * @ORM\Column(name="thigh", type="float", nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $thigh;

    /**
     * @var string
     *
     * @ORM\Column(name="thighUnit", type="string", length=10, nullable=true)
     * 
     * @Assert\Choice(
     * 	choices={"cm", "in"}
     * )
     * 
     * @SerializedName("thighUnit")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $thighUnit;

    /**
     * @var float
     *
     * @ORM\Column(name="calf", type="float", nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $calf;

    /**
     * @var string
     *
     * @ORM\Column(name="calfUnit", type="string", length=10, nullable=true)
     * 
     * @Assert\Choice(
     * 	choices={"cm", "in"}
     * )
     * 
     * @SerializedName("calfUnit")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $calfUnit;

    /**
     * @var float
     *
     * @ORM\Column(name="hip", type="float", nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $hip;

    /**
     * @var string
     *
     * @ORM\Column(name="hipUnit", type="string", length=10, nullable=true)
     * 
     * @Assert\Choice(
     * 	choices={"cm", "in"}
     * )
     * 
     * @SerializedName("hipUnit")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $hipUnit;

    /**
     * @var float
     *
     * @ORM\Column(name="upperArm", type="float", nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @SerializedName("upperArm")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $upperArm;

    /**
     * @var string
     *
     * @ORM\Column(name="upperArmUnit", type="string", length=10, nullable=true)
     * 
     * @Assert\Choice(
     * 	choices={"cm", "in"}
     * )
     * 
     * @SerializedName("upperArmUnit")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $upperArmUnit;

    /**
     * @var float
     *
     * @ORM\Column(name="bmi", type="float", nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $bmi;

    /**
     * @var float
     *
     * @ORM\Column(name="bmr", type="float", nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $bmr;

    /**
     * @var float
     *
     * @ORM\Column(name="targetWeight", type="float", nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @SerializedName("targetWeight")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $targetWeight;

    /**
     * @var float
     *
     * @ORM\Column(name="targetHeartRate", type="float", nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @SerializedName("targetHeartRate")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $targetHeartRate;

    /**
     * @var float
     *
     * @ORM\Column(name="bodyFat", type="float", nullable=true)
     * 
     * @Assert\GreaterThanOrEqual(
     * 	value = 0
     * )
     * 
     * @SerializedName("bodyFat")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $bodyFat;

    /**
     * @var string
     *
     * @ORM\Column(name="sidePhoto", type="string", length=255, nullable=true)
     * 
     * @SerializedName("sidePhoto")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $sidePhoto;

    /**
     * @var string
     *
     * @ORM\Column(name="frontPhoto", type="string", length=255, nullable=true)
     * 
     * @SerializedName("frontPhoto")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $frontPhoto;

    /**
     * @var string
     *
     * @ORM\Column(name="backPhoto", type="string", length=255, nullable=true)
     * 
     * @SerializedName("backPhoto")
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $backPhoto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     * @Assert\DateTime()
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $date;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="intensity", type="string", nullable=true)
     * @Assert\Choice({"extreme", "high", "medium", "low"})
     * 
     * @Groups({"stat-list", "stat-details"})
     * 
     * @Expose
     */
    private $intensity;

    /**
     * Constructor
     */
    public function __construct(){
    	$this->setDate(new \DateTime());
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


	/**
	 * @return User
	 */
	public function getUser() {
		return $this->user;
	}
	
	/**
	 * @param User $user
	 */
	public function setUser(User $user) {
		$this->user = $user;
		return $this;
	}
	
    /**
     * Set height
     *
     * @param float $height
     * @return Stat
     */
    public function setHeight($height)
    {
        $this->height = $height;
    
        return $this;
    }

    /**
     * Get height
     * 
     * @return float 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set heightUnit
     *
     * @param string $heightUnit
     * @return Stat
     */
    public function setHeightUnit($heightUnit)
    {
        $this->heightUnit = $heightUnit;
    
        return $this;
    }

    /**
     * Get heightUnit
     *
     * @return string 
     */
    public function getHeightUnit()
    {
        return $this->heightUnit;
    }

    /**
     * Set weight
     *
     * @param float $weight
     * @return Stat
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    
        return $this;
    }

    /**
     * Get weight
     *
     * @return float 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set weightUnit
     *
     * @param string $weightUnit
     * @return Stat
     */
    public function setWeightUnit($weightUnit)
    {
        $this->weightUnit = $weightUnit;
    
        return $this;
    }

    /**
     * Get weightUnit
     *
     * @return string 
     */
    public function getWeightUnit()
    {
        return $this->weightUnit;
    }

    /**
     * Set restingHeartRate
     *
     * @param float $restingHeartRate
     * @return Stat
     */
    public function setRestingHeartRate($restingHeartRate)
    {
        $this->restingHeartRate = $restingHeartRate;
    
        return $this;
    }

    /**
     * Get restingHeartRate
     *
     * @return float 
     */
    public function getRestingHeartRate()
    {
        return $this->restingHeartRate;
    }

    /**
     * Set neck
     *
     * @param string $neck
     * @return Stat
     */
    public function setNeck($neck)
    {
        $this->neck = $neck;
    
        return $this;
    }

    /**
     * Get neck
     *
     * @return string 
     */
    public function getNeck()
    {
        return $this->neck;
    }

    /**
     * Set neckUnit
     *
     * @param string $neckUnit
     * @return Stat
     */
    public function setNeckUnit($neckUnit)
    {
        $this->neckUnit = $neckUnit;
    
        return $this;
    }

    /**
     * Get neckUnit
     *
     * @return string 
     */
    public function getNeckUnit()
    {
        return $this->neckUnit;
    }

    /**
     * Set chest
     *
     * @param float $chest
     * @return Stat
     */
    public function setChest($chest)
    {
        $this->chest = $chest;
    
        return $this;
    }

    /**
     * Get chest
     *
     * @return float 
     */
    public function getChest()
    {
        return $this->chest;
    }

    /**
     * Set chestUnit
     *
     * @param string $chestUnit
     * @return Stat
     */
    public function setChestUnit($chestUnit)
    {
        $this->chestUnit = $chestUnit;
    
        return $this;
    }

    /**
     * Get chestUnit
     *
     * @return string 
     */
    public function getChestUnit()
    {
        return $this->chestUnit;
    }

    /**
     * Set thigh
     *
     * @param float $thigh
     * @return Stat
     */
    public function setThigh($thigh)
    {
        $this->thigh = $thigh;
    
        return $this;
    }

    /**
     * Get thigh
     *
     * @return float 
     */
    public function getThigh()
    {
        return $this->thigh;
    }

    /**
     * Set thighUnit
     *
     * @param string $thighUnit
     * @return Stat
     */
    public function setThighUnit($thighUnit)
    {
        $this->thighUnit = $thighUnit;
    
        return $this;
    }

    /**
     * Get thighUnit
     *
     * @return string 
     */
    public function getThighUnit()
    {
        return $this->thighUnit;
    }

    /**
     * Set calf
     *
     * @param float $calf
     * @return Stat
     */
    public function setCalf($calf)
    {
        $this->calf = $calf;
    
        return $this;
    }

    /**
     * Get calf
     *
     * @return float 
     */
    public function getCalf()
    {
        return $this->calf;
    }

    /**
     * Set calfUnit
     *
     * @param string $calfUnit
     * @return Stat
     */
    public function setCalfUnit($calfUnit)
    {
        $this->calfUnit = $calfUnit;
    
        return $this;
    }

    /**
     * Get calfUnit
     *
     * @return string 
     */
    public function getCalfUnit()
    {
        return $this->calfUnit;
    }

    /**
     * Set hip
     *
     * @param float $hip
     * @return Stat
     */
    public function setHip($hip)
    {
        $this->hip = $hip;
    
        return $this;
    }

    /**
     * Get hip
     *
     * @return float 
     */
    public function getHip()
    {
        return $this->hip;
    }

    /**
     * Set hipUnit
     *
     * @param string $hipUnit
     * @return Stat
     */
    public function setHipUnit($hipUnit)
    {
        $this->hipUnit = $hipUnit;
    
        return $this;
    }

    /**
     * Get hipUnit
     *
     * @return string 
     */
    public function getHipUnit()
    {
        return $this->hipUnit;
    }

    /**
     * Set upperArm
     *
     * @param float $upperArm
     * @return Stat
     */
    public function setUpperArm($upperArm)
    {
        $this->upperArm = $upperArm;
    
        return $this;
    }

    /**
     * Get upperArm
     *
     * @return float 
     */
    public function getUpperArm()
    {
        return $this->upperArm;
    }

    /**
     * Set upperArmUnit
     *
     * @param string $upperArmUnit
     * @return Stat
     */
    public function setUpperArmUnit($upperArmUnit)
    {
        $this->upperArmUnit = $upperArmUnit;
    
        return $this;
    }

    /**
     * Get upperArmUnit
     *
     * @return string 
     */
    public function getUpperArmUnit()
    {
        return $this->upperArmUnit;
    }

    /**
     * Set bmi
     *
     * @param float $bmi
     * @return Stat
     */
    public function setBmi($bmi)
    {
        $this->bmi = $bmi;
    
        return $this;
    }

    /**
     * Get bmi
     *
     * @return float 
     */
    public function getBmi()
    {
        return $this->bmi;
    }

    /**
     * Set bmr
     *
     * @param float $bmr
     * @return Stat
     */
    public function setBmr($bmr)
    {
        $this->bmr = $bmr;
    
        return $this;
    }

    /**
     * Get bmr
     *
     * @return float 
     */
    public function getBmr()
    {
        return $this->bmr;
    }

    /**
     * Set targetWeight
     *
     * @param float $targetWeight
     * @return Stat
     */
    public function setTargetWeight($targetWeight)
    {
        $this->targetWeight = $targetWeight;
    
        return $this;
    }

    /**
     * Get targetWeight
     *
     * @return float 
     */
    public function getTargetWeight()
    {
        return $this->targetWeight;
    }

    /**
     * Set targetHeartRate
     *
     * @param float $targetHeartRate
     * @return Stat
     */
    public function setTargetHeartRate($targetHeartRate)
    {
        $this->targetHeartRate = $targetHeartRate;
    
        return $this;
    }

    /**
     * Get targetHeartRate
     *
     * @return float 
     */
    public function getTargetHeartRate()
    {
        return $this->targetHeartRate;
    }

    /**
     * Set bodyFat
     *
     * @param float $bodyFat
     * @return Stat
     */
    public function setBodyFat($bodyFat)
    {
        $this->bodyFat = $bodyFat;
    
        return $this;
    }

    /**
     * Get bodyFat
     *
     * @return float 
     */
    public function getBodyFat()
    {
        return $this->bodyFat;
    }

    /**
     * Set sidePhoto
     *
     * @param string $sidePhoto
     * @return Stat
     */
    public function setSidePhoto($sidePhoto)
    {
        $this->sidePhoto = $sidePhoto;
    
        return $this;
    }

    /**
     * Get sidePhoto
     *
     * @return string 
     */
    public function getSidePhoto()
    {
        return $this->sidePhoto;
    }

    /**
     * Set frontPhoto
     *
     * @param string $frontPhoto
     * @return Stat
     */
    public function setFrontPhoto($frontPhoto)
    {
        $this->frontPhoto = $frontPhoto;
    
        return $this;
    }

    /**
     * Get frontPhoto
     *
     * @return string 
     */
    public function getFrontPhoto()
    {
        return $this->frontPhoto;
    }

    /**
     * Set backPhoto
     *
     * @param string $backPhoto
     * @return Stat
     */
    public function setBackPhoto($backPhoto)
    {
        $this->backPhoto = $backPhoto;
    
        return $this;
    }

    /**
     * Get backPhoto
     *
     * @return string 
     */
    public function getBackPhoto()
    {
        return $this->backPhoto;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Stat
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

	/**
	 * @return the string
	 */
	public function getIntensity() {
		return $this->intensity;
	}
	
	/**
	 * @param string $intensity
	 */
	public function setIntensity($intensity) {
		$this->intensity = $intensity;
		return $this;
	}
	
}
