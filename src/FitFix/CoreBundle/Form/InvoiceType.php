<?php

namespace FitFix\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FitFix\CoreBundle\Form\DataTransformer\StringToDateTimeTransformer;

class InvoiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	
        $builder
            ->add(
            		$builder->create('paid', 'text')->addModelTransformer(new StringToDateTimeTransformer())
        	)
            ->add('price')
            ->add('status')
            ->add(
            		$builder->create('createdAt', 'text')->addModelTransformer(new StringToDateTimeTransformer())
            )
            ->add(
            		$builder->create('updatedAt', 'text')->addModelTransformer(new StringToDateTimeTransformer())
            )
            ->add('description')
            ->add('from')
            ->add('to')
            ->add('tag')
            ->add('paymentMethod')
            ->add('uuid', 'text', array('mapped' => false))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FitFix\CoreBundle\Entity\Invoice'
        ));
    }

    public function getName()
    {
        return '';
    }
}
