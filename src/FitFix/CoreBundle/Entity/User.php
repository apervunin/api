<?php

namespace FitFix\CoreBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"user-details", "details", "event-details", "event-list", "invoice-details", "stat-details"})
     */
    protected $id;

    /**
     * @Expose
     * @Groups({"user-details", "details", "event-details", "invoice-details"})
     */
    protected $username;

    /**
     * @Expose
     * @Groups({"user-details", "details", "event-details", "invoice-details"})
     */
    protected $email;

    /**
     * @var \Client
     *
     * @ORM\OneToOne(targetEntity="FitFix\CoreBundle\Entity\Client", mappedBy="user", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\Valid(
     *     traverse=false
     * )
     * 
     * @Expose
     * @Groups({"event-details", "notification", "invoice-details"})
     */
    private $client;

    /**
     * @var \Trainer
     *
     * @ORM\OneToOne(targetEntity="FitFix\CoreBundle\Entity\Trainer", mappedBy="user", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="trainer_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\Valid(
     *     traverse=false
     * )
     * 
     * @Expose
     * @Groups({"event-details", "notification", "invoice-details"})
     */
    private $trainer;
    
    /**
     * @var string
     *
     * @ORM\Column(type="datetime", nullable=true, name="registered")
     * @Assert\NotBlank(
     *     message="Please enter a registered date/time (YYYY-MM-DD HH:MM:SS)"
     * )
     * @Assert\DateTime(
     *     message="Please enter a valid paid date/time (YYYY-MM-DD HH:MM:SS)"
     * )
     * @Expose
     * @Groups({"details", "event-details"})
     */
    private $registered;
    
    public function __construct(){
    	parent::__construct();
    	$this->registered = new \DateTime(); 	
    }
    
    public function setEmail($email){
    	$this->setUsername($email);
    	return parent::setEmail($email);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param \FitFix\CoreBundle\Entity\Client $client
     * @return User
     */
    public function setClient(\FitFix\CoreBundle\Entity\Client $client = null)
    {
        $client->setUser($this);

        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \FitFix\CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set trainer
     *
     * @param \FitFix\CoreBundle\Entity\Trainer $trainer
     * @return User
     */
    public function setTrainer(\FitFix\CoreBundle\Entity\Trainer $trainer = null)
    {
        $this->trainer = $trainer;

        return $this;
    }

    /**
     * Get trainer
     *
     * @return \FitFix\CoreBundle\Entity\Trainer
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Set registered
     *
     * @param \DateTime $registered
     * @return User
     */
    public function setRegistered($registered)
    {
        $this->registered = $registered;
    
        return $this;
    }

    /**
     * Get registered
     *
     * @return \DateTime 
     */
    public function getRegistered()
    {
        return $this->registered;
    }
}
