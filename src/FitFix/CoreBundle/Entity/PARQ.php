<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Rhumsaa\Uuid\Uuid;

/**
 * PARQ
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\PARQRepository")
 * @ExclusionPolicy("all")
 */
class PARQ
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var UUID
     * 
     * @ORM\Column(type="string", length=255)
     */
    private $uuid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     * @Expose
     */
    private $active;

    /**
     * @var boolean
     *
     * @ORM\Column(name="alcohol", type="boolean", nullable=true)
     * @Expose
     */
    private $alcohol;

    /**
     * @var string
     *
     * @ORM\Column(name="blood", type="text", nullable=false)
     * @Expose
     */
    private $blood;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bloodpress", type="boolean", nullable=true)
     * @Expose
     */
    private $bloodpress;

    /**
     * @var boolean
     *
     * @ORM\Column(name="caredoc", type="boolean", nullable=false)
     * @Expose
     */
    private $caredoc;

    /**
     * @var string
     *
     * @ORM\Column(name="caredocIfYes", type="text", nullable=true)
     * @Expose
     */
    private $caredocIfYes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cholesterol", type="boolean", nullable=true)
     * @Expose
     */
    private $cholesterol;

    /**
     * @var string
     *
     * @ORM\Column(name="contactaddr", type="text", nullable=true)
     * @Expose
     */
    private $contactaddr;

    /**
     * @var string
     *
     * @ORM\Column(name="contactname", type="text", nullable=true)
     * @Expose
     */
    private $contactname;

    /**
     * @var int
     *
     * @ORM\Column(name="contactnum", type="integer", nullable=true)
     * @Expose
     */
    private $contactnum;

    /**
     * @var string
     *
     * @ORM\Column(name="contactrel", type="text", nullable=true)
     * @Expose
     */
    private $contactrel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="diabetes", type="boolean", nullable=true)
     */
    private $diabetes;

    /**
     * @var int
     *
     * @ORM\Column(name="height", type="integer", nullable=false)
     * @Assert\GreaterThan(
     * 	value = 5,
     * 	message = "Please enter a realistic weight"
     * )
     * @Assert\NotBlank()
     */
    private $height;

    /**
     * @var string
     *
     * @ORM\Column(name="heightUnit", type="text", nullable=false)
     * @Assert\Choice(
     * 	choices = {
     * 		"cm",
     * 		"in"
     * 	},
     * 	message = "Please choose a valid weight unit (cm/in)"
     * )
     * @Assert\NotBlank()
     */
    private $heightUnit;

    /**
     * @var string
     *
     * @ORM\Column(name="lastcheck", type="text", nullable=true)
     */
    private $lastcheck;

    /**
     * @var boolean
     *
     * @ORM\Column(name="medications", type="boolean", nullable=true)
     */
    private $medications;

    /**
     * @var string
     *
     * @ORM\Column(name="medicationsIfYes", type="text", nullable=true)
     */
    private $medicationsIfYes;

    /**
     * @var string
     *
     * @ORM\Column(name="physaddr", type="text", nullable=true)
     */
    private $physaddr;

    /**
     * @var string
     *
     * @ORM\Column(name="physname", type="text", nullable=true)
     */
    private $physname;

    /**
     * @var int
     *
     * @ORM\Column(name="physnum", type="integer", nullable=true)
     */
    private $physnum;

    /**
     * @var string
     *
     * @ORM\Column(name="physrel", type="text", nullable=true)
     */
    private $physrel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pregnant", type="boolean", nullable=true)
     */
    private $pregnant;

    /**
     * @var boolean
     *
     * @ORM\Column(name="smoker", type="boolean", nullable=true)
     */
    private $smoker;

    /**
     * @var boolean
     *
     * @ORM\Column(name="stress", type="boolean", nullable=true)
     */
    private $stress;

    /**
     * @var string
     *
     * @ORM\Column(name="stresstest", type="string", nullable=false)
     * @Assert\Choice(
     * 	choices = {
     * 		"yes",
     * 		"no",
     * 		"dk"
     * 	},
     * 	message = "Please choose a valid stress test (yes/no/dk)"
     * )
     * @Assert\NotBlank()
     */
    private $stresstest;

    /**
     * @var string
     *
     * @ORM\Column(name="stresstestResult", type="string", nullable=true)
     * @Assert\Choice(
     * 	choices = {
     * 		"abnormal",
     * 		"normal"
     * 	},
     * 	message = "Please choose a valid stress test result (abnormal/normal)"
     * )
     */
    private $stresstestResult;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="integer", nullable=false)
     * @Assert\GreaterThan(
     * 	value = 40,
     * 	message = "Please enter a realistic weight"
     * )
     * @Assert\NotBlank()
     */
    private $weight;

    /**
     * @var string
     *
     * @ORM\Column(name="weightUnit", type="string", nullable=false)
     * @Assert\Choice(
     * 	choices = {
     * 		"lbs",
     * 		"kg"
     * 	},
     * 	message = "Please choose a valid weight unit (lbs/kg)"
     * )
     * @Assert\NotBlank()
     */
    private $weightUnit;

    
    public function __construct(){
    	$this->uuid = Uuid::uuid4() . '-' . Uuid::uuid4();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return PARQ
     */
    public function setActive($active)
    {
        $this->active = self::strToBool($active);
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set alcohol
     *
     * @param boolean $alcohol
     * @return PARQ
     */
    public function setAlcohol($alcohol)
    {
        $this->alcohol = self::strToBool($alcohol);
    
        return $this;
    }

    /**
     * Get alcohol
     *
     * @return boolean 
     */
    public function getAlcohol()
    {
        return $this->alcohol;
    }

    /**
     * Set blood
     *
     * @param string $blood
     * @return PARQ
     */
    public function setBlood($blood)
    {
        $this->blood = $blood;
    
        return $this;
    }

    /**
     * Get blood
     *
     * @return string 
     */
    public function getBlood()
    {
        return $this->blood;
    }

    /**
     * Set bloodpress
     *
     * @param boolean $bloodpress
     * @return PARQ
     */
    public function setBloodpress($bloodpress)
    {
        $this->bloodpress = self::strToBool($bloodpress);
    
        return $this;
    }

    /**
     * Get bloodpress
     *
     * @return boolean 
     */
    public function getBloodpress()
    {
        return $this->bloodpress;
    }

    /**
     * Set caredoc
     *
     * @param boolean $caredoc
     * @return PARQ
     */
    public function setCaredoc($caredoc)
    {
        $this->caredoc = self::strToBool($caredoc);
    
        return $this;
    }

    /**
     * Get caredoc
     *
     * @return boolean 
     */
    public function getCaredoc()
    {
        return $this->caredoc;
    }

    /**
     * Set caredoc_if_yes
     *
     * @param string $caredocIfYes
     * @return PARQ
     */
    public function setCaredocIfYes($caredocIfYes)
    {
        $this->caredocIfYes = $caredocIfYes;
    
        return $this;
    }

    /**
     * Get caredoc_if_yes
     *
     * @return string 
     */
    public function getCaredocIfYes()
    {
        return $this->caredocIfYes;
    }

    /**
     * Set cholesterol
     *
     * @param boolean $cholesterol
     * @return PARQ
     */
    public function setCholesterol($cholesterol)
    {
        $this->cholesterol = self::strToBool($cholesterol);
    
        return $this;
    }

    /**
     * Get cholesterol
     *
     * @return boolean 
     */
    public function getCholesterol()
    {
        return $this->cholesterol;
    }

    /**
     * Set contactaddr
     *
     * @param string $contactaddr
     * @return PARQ
     */
    public function setContactaddr($contactaddr)
    {
        $this->contactaddr = $contactaddr;
    
        return $this;
    }

    /**
     * Get contactaddr
     *
     * @return string 
     */
    public function getContactaddr()
    {
        return $this->contactaddr;
    }

    /**
     * Set contactname
     *
     * @param string $contactname
     * @return PARQ
     */
    public function setContactname($contactname)
    {
        $this->contactname = $contactname;
    
        return $this;
    }

    /**
     * Get contactname
     *
     * @return string 
     */
    public function getContactname()
    {
        return $this->contactname;
    }

    /**
     * Set contactnum
     *
     * @param $contactnum
     * @return PARQ
     */
    public function setContactnum($contactnum)
    {
        $this->contactnum = $contactnum;
    
        return $this;
    }

    /**
     * Get contactnum
     *
     * @return 
     */
    public function getContactnum()
    {
        return $this->contactnum;
    }

    /**
     * Set contactrel
     *
     * @param string $contactrel
     * @return PARQ
     */
    public function setContactrel($contactrel)
    {
        $this->contactrel = $contactrel;
    
        return $this;
    }

    /**
     * Get contactrel
     *
     * @return string 
     */
    public function getContactrel()
    {
        return $this->contactrel;
    }

    /**
     * Set diabetes
     *
     * @param boolean $diabetes
     * @return PARQ
     */
    public function setDiabetes($diabetes)
    {
        $this->diabetes = self::strToBool($diabetes);
    
        return $this;
    }

    /**
     * Get diabetes
     *
     * @return boolean 
     */
    public function getDiabetes()
    {
        return $this->diabetes;
    }

    /**
     * Set height
     *
     * @param $height
     * @return PARQ
     */
    public function setHeight($height)
    {
        $this->height = $height;
    
        return $this;
    }

    /**
     * Get height
     *
     * @return 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set height_unit
     *
     * @param string $heightUnit
     * @return PARQ
     */
    public function setHeightUnit($heightUnit)
    {
        $this->heightUnit = $heightUnit;
    
        return $this;
    }

    /**
     * Get height_unit
     *
     * @return string 
     */
    public function getHeightUnit()
    {
        return $this->heightUnit;
    }

    /**
     * Set lastcheck
     *
     * @param string $lastcheck
     * @return PARQ
     */
    public function setLastcheck($lastcheck)
    {
        $this->lastcheck = $lastcheck;
    
        return $this;
    }

    /**
     * Get lastcheck
     *
     * @return string 
     */
    public function getLastcheck()
    {
        return $this->lastcheck;
    }

    /**
     * Set medications
     *
     * @param boolean $medications
     * @return PARQ
     */
    public function setMedications($medications)
    {
        $this->medications = self::strToBool($medications);
    
        return $this;
    }

    /**
     * Get medications
     *
     * @return boolean 
     */
    public function getMedications()
    {
        return $this->medications;
    }

    /**
     * Set medications_if_yes
     *
     * @param string $medicationsIfYes
     * @return PARQ
     */
    public function setMedicationsIfYes($medicationsIfYes)
    {
        $this->medicationsIfYes = $medicationsIfYes;
    
        return $this;
    }

    /**
     * Get medications_if_yes
     *
     * @return string 
     */
    public function getMedicationsIfYes()
    {
        return $this->medicationsIfYes;
    }

    /**
     * Set physaddr
     *
     * @param string $physaddr
     * @return PARQ
     */
    public function setPhysaddr($physaddr)
    {
        $this->physaddr = $physaddr;
    
        return $this;
    }

    /**
     * Get physaddr
     *
     * @return string 
     */
    public function getPhysaddr()
    {
        return $this->physaddr;
    }

    /**
     * Set physname
     *
     * @param string $physname
     * @return PARQ
     */
    public function setPhysname($physname)
    {
        $this->physname = $physname;
    
        return $this;
    }

    /**
     * Get physname
     *
     * @return string 
     */
    public function getPhysname()
    {
        return $this->physname;
    }

    /**
     * Set physnum
     *
     * @param $physnum
     * @return PARQ
     */
    public function setPhysnum($physnum)
    {
        $this->physnum = $physnum;
    
        return $this;
    }

    /**
     * Get physnum
     *
     * @return 
     */
    public function getPhysnum()
    {
        return $this->physnum;
    }

    /**
     * Set physrel
     *
     * @param string $physrel
     * @return PARQ
     */
    public function setPhysrel($physrel)
    {
        $this->physrel = $physrel;
    
        return $this;
    }

    /**
     * Get physrel
     *
     * @return string 
     */
    public function getPhysrel()
    {
        return $this->physrel;
    }

    /**
     * Set pregnant
     *
     * @param boolean $pregnant
     * @return PARQ
     */
    public function setPregnant($pregnant)
    {
        $this->pregnant = self::strToBool($pregnant);
    
        return $this;
    }

    /**
     * Get pregnant
     *
     * @return boolean 
     */
    public function getPregnant()
    {
        return $this->pregnant;
    }

    /**
     * Set smoker
     *
     * @param boolean $smoker
     * @return PARQ
     */
    public function setSmoker($smoker)
    {
        $this->smoker = self::strToBool($smoker);
    
        return $this;
    }

    /**
     * Get smoker
     *
     * @return boolean 
     */
    public function getSmoker()
    {
        return $this->smoker;
    }

    /**
     * Set stress
     *
     * @param boolean $stress
     * @return PARQ
     */
    public function setStress($stress)
    {
        $this->stress = self::strToBool($stress);
    
        return $this;
    }

    /**
     * Get stress
     *
     * @return boolean 
     */
    public function getStress()
    {
        return $this->stress;
    }

    /**
     * Set stresstest
     *
     * @param string $stresstest
     * @return PARQ
     */
    public function setStresstest($stresstest)
    {
        $this->stresstest = $stresstest;
    
        return $this;
    }

    /**
     * Get stresstest
     *
     * @return string 
     */
    public function getStresstest()
    {
        return $this->stresstest;
    }

    /**
     * Set stresstest_result
     *
     * @param string $stresstestResult
     * @return PARQ
     */
    public function setStresstestResult($stresstestResult)
    {
        $this->stresstestResult = $stresstestResult;
    
        return $this;
    }

    /**
     * Get stresstest_result
     *
     * @return string 
     */
    public function getStresstestResult()
    {
        return $this->stressTest;
    }

    /**
     * Set weight
     *
     * @param $weight
     * @return PARQ
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    
        return $this;
    }

    /**
     * Get weight
     *
     * @return 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set weight_unit
     *
     * @param string $weightUnit
     * @return PARQ
     */
    public function setWeightUnit($weightUnit)
    {
        $this->weightUnit = $weightUnit;
    
        return $this;
    }

    /**
     * Get weight_unit
     *
     * @return string 
     */
    public function getWeightUnit()
    {
        return $this->weightUnit;
    }
    
    private static function strToBool($string){
    	return ($string == "yes") ? true : false;
    }
    

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return PARQ
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    
        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }
}