define([
    'jquery',
    'handlebars',
    'backbone',
    'models/session',
    'models/client',
    'text!templates/client/page.html',
], function($, _, Backbone, Session, ClientModel, clientPageTemplate){
    var ClientPage = Backbone.View.extend({
        el: '.page',
        render: function () {
            that = this;

            // Here we have set the `id` of the model
            that.client = new ClientModel({id: 1});

            that.wsse = Session.get('WSSE');

            // The fetch below will perform GET /clients/1
            that.client.fetch({
                success: function (client) {
                    var data = { client: client.toJSON() };
                    //console.log(data);
                    var compiledTemplate = Handlebars.compile(clientPageTemplate);
                    var html = compiledTemplate(data);
                    $(that.el).html(html);
                },
                headers: {
                        'Authorization' :'WSSE profile="UsernameToken"',
                        'X-wsse' : that.wsse,
                        'Accept' : 'application/json'
                    }
            })


            //this.collection.add({ name: "Ginger Kid"});
            // Using Underscore we can compile our template with data

        }
    });
    return ClientPage;
});