<?php

namespace FitFix\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment')
            ->add('nutritionPlanning')
            ->add('punctuality')
            ->add('availability')
            ->add('invoicingManagement')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FitFix\CoreBundle\Entity\Review'
        ));
    }

    public function getName()
    {
        return 'fitfix_adminbundle_reviewtype';
    }
}
