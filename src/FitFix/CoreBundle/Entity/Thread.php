<?php

namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * Thread
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\ThreadRepository")
 * 
 * @ExclusionPolicy("all")
 */
class Thread
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @Expose()
     * @Groups({"details"})
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Trainer", inversedBy="threads", cascade={"persist"})
     * @ORM\JoinColumn(name="trainer_id", referencedColumnName="id")
     * 
     * @Expose()
     * @Groups({"details"})
     */
    protected $trainer;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FitFix\CoreBundle\Entity\Message", mappedBy="thread", cascade={"persist", "remove"})
     * 
     * @Expose()
     * @Groups({"details"})
     */
    private $messages;

    /**
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Client", inversedBy="threads", cascade={"persist"})
     * 
     * @Expose()
     * @Groups({"details"})
     */
    protected $client;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getMessages()
    {
        return $this->messages;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }
 

    /**
     * Set trainer
     *
     * @param \FitFix\CoreBundle\Entity\Trainer $trainer
     * @return Thread
     */
    public function setTrainer(\FitFix\CoreBundle\Entity\Trainer $trainer = null)
    {
        $this->trainer = $trainer;
    
        return $this;
    }

    /**
     * Get trainer
     *
     * @return \FitFix\CoreBundle\Entity\Trainer 
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Add messages
     *
     * @param \FitFix\CoreBundle\Entity\Message $messages
     * @return Thread
     */
    public function addMessage(\FitFix\CoreBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;
    
        return $this;
    }

    /**
     * Remove messages
     *
     * @param \FitFix\CoreBundle\Entity\Message $messages
     */
    public function removeMessage(\FitFix\CoreBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Set client
     *
     * @param \FitFix\CoreBundle\Entity\Client $client
     * @return Thread
     */
    public function setClient(\FitFix\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;
    
        return $this;
    }

    /**
     * Get client
     *
     * @return \FitFix\CoreBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }
}