<?php

namespace FitFix\ApiBundle\Tests\Controller;

use FitFix\CoreBundle\Entity\Client;
use Symfony\Component\HttpKernel\Kernel;

use Symfony\Component\BrowserKit\Client as BrowserClient;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test class for goal rest controller
 *
 */
class GoalRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var BrowserClient
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var EntityManager
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {
        $this->client = static::createClient();
        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');

        $this->em = static::$kernel->getContainer() ->get('doctrine') ->getEntityManager();

        /*
         * Creation of the browser client with the client1 authenticated header
         */
        $user = $this->userManager->findUserByUsername("client1");
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );
            $this->client->setServerParameters($this->header);
         }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get goals
     *
     * @return none
     */
    public function testGetGoalsAction_valid_user()
    {
        $this->client->request('GET', '/api/clients/client1/goals');
        $content = $this->client->getResponse()->getContent();
        $goals = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(1, $goals);
    }

    /**
     * Test get goal
     *
     * @return none
     */
    public function testGetGoalAction_valid_user()
    {
        $this->client->request('GET', '/api/clients/client1/goals/1');
        $content = $this->client->getResponse()->getContent();
        $goal = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertEquals('Get Fit', $goal->description);
    }

    /**
     * Test get client nonexistent slug
     *
     * @return none
     */
    public function testGetGoalAction_invalid_client()
    {
        $this->client->request('GET', '/api/clients/client0/goals');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get client unauthorised slug
     *
     * @return none
     */
    public function testGetGoalAction_unauthorised_client()
    {
        $this->client->request('GET', '/api/clients/client2/goals');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test create goal
     *
     * @return none
     */
    public function testPostGoalsAction()
    {

        $params = array(
            'description'      => 'Eat Well'
        );

        $this->client->request('POST', '/api/clients/client1/goals', $params);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $this->assertRegExp('/\/api\/clients\/client1\/goals\/3/', $this->client->getResponse()->headers->get('location'));

        $goal = $this->em->getRepository('FitFixCoreBundle:Goal')->find(3);
        $this->assertNotNull($goal, "Goal created");
        if ($goal) {
            $this->em->remove($goal);
            $this->em->flush();
        }
    }

    /**
     * Test create goal invalid data
     *
     * @return none
     */
    public function testPostGoalsAction_invalid_data()
    {
        $params = array(
            'description'      => ''
        );

        $this->client->request('POST', '/api/clients/client1/goals', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"description":["Please enter a description"]}';
        $this->assertEquals($expected, $this->client->getResponse()->getContent());

    }

    /**
     * Test delete an existing goal
     *
     * @return none
     */
    public function testDeleteGoalAction()
    {
        $this->client->request('DELETE', '/api/clients/client1/goals/2');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete goal nonexistent
     *
     * @return none
     */
    public function testDeleteGoalAction_invalid_goal()
    {
        $this->client->request('DELETE', '/api/clients/client1/goals/0');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete goal without id
     *
     * @return none
     */
    public function testDeleteGoalAction_without_id()
    {
        $this->client->request('DELETE', '/api/clients/client1/goals');

        $this->assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update goal
     *
     * @return none
     */
    public function testPutGoalAction()
    {
        $params = array('description' => 'Eat fresh fruit');

        $this->client->request('PUT', '/api/clients/client1/goals/1', $params);

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }

    /**
     * Test update goal invalid id
     *
     * @return none
     */
    public function testPutGoalAction_invalid_id()
    {
        $params = array('description' => 'Eat fresh fruit');

        $this->client->request('PUT', '/api/clients/client1/goals/0', $params);

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update goal unauthorised slug
     *
     * @return none
     */
    public function testPutGoalAction_unauthorised_client()
    {
        $params = array('description' => 'Eat fresh fruit');

        $this->client->request('GET', '/api/clients/client2/goals/1', $params);

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get options
     *
     * @return none
     */
    public function testOptionsGoalsAction()
    {
        $this->client->request('OPTIONS', '/api/client/goals', array(), array(), $this->header);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('access-control-allow-methods', 'OPTIONS, GET, POST, PUT, DELETE'));
    }
}
