<?php

namespace FitFix\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SessionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startTime', 'datetime')
            ->add('endTime', 'datetime')
            ->add('requestStatus')
            ->add('trainer', 'entity', array(
                'class' => 'FitFixCoreBundle:Trainer',
                'multiple' => false
            ))
            ->add('clients', 'entity', array(
                'class' => 'FitFixCoreBundle:Client',
                'multiple' => true
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FitFix\CoreBundle\Entity\Session'
        ));
    }

    public function getName()
    {
        return 'fitfix_adminbundle_sessiontype';
    }
}
