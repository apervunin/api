<?php
// src/FitFix/CoreBundle/Admin/ClientAdmin.php

namespace FitFix\CoreBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ClientAdmin extends Admin
{

   
    protected $baseRouteName = 'fitfix_admin_client';

    protected $baseRoutePattern = 'client';
    
    // setup the defaut sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'id'
    );

    //Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('firstName', 'text', array('label' => 'title'))
                ->add('lastname', 'text')
                ->add('dob', 'date')
                ->add('landline', 'text')
                ->add('mobile', 'text')
                ->add('address', 'text')
                ->add('archived', 'checkbox')
                ->add('notes', 'textarea')
                ->add('specificAim', 'textarea')
                
                ->setHelps(array(
                    'title' => 'title'
        ));
    }
    
    protected function configureShowFields(ShowMapper $showMapper) {
                
        $showMapper
                ->add('id')
                ->add('firstName')
                //->add('country')
                //->add('fips')
                ;
    }

    //Fields to be shown on filter formss e vtyt d 
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('firstName')
                //->add('country')
                ;
    }

    //Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id', null, array('route' => array('name' => 'show')))
                ->add('firstName')
                ->add('lastname')
                ->add('_action', 'actions', array(
                        'actions' => array(
                            'show' => array(),
                            'edit' => array(),
                            'delete' => array(),
                        )
        ));
    }
}