<?php

namespace FitFix\ApiBundle\Controller;


use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;

/**
 * Controller that provides Restful services over the resource Thread.
 *
 * @NamePrefix("fitfix_api_threadrest_")
 * @author Daniele Longo <daniele.longo.development@gmail.com>
 */
class ThreadRestController extends Controller
{

    /**
     * Returns all trainers.
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getThreadsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $authenticatedUser = $this->get('security.context')->getToken()->getUser();

        $view = FOSView::create();

        $roles = $authenticatedUser->getRoles();

        if (in_array('ROLE_CLIENT', $roles)) {
            $client = $authenticatedUser->getClient();
            $entities = $em->getRepository('FitFixCoreBundle:Thread')->findByClient($client);
        }
        elseif (in_array('ROLE_TRAINER', $roles)) {
            $trainer = $authenticatedUser->getTrainer();
            $entities = $em->getRepository('FitFixCoreBundle:Thread')->findByTrainer($trainer);
        }
        	
        if ($entities) {
            $serializer = $this->container->get('serializer');
            
            $serializationContext = SerializationContext::create()->setGroups(array('list'));
            $view->setSerializationContext($serializationContext);
            $view->setStatusCode(200)->setData($entities);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }
}