<?php

namespace FitFix\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\Kernel;

use Symfony\Component\BrowserKit\Client as BrowserClient;

/**
 * Test class for user rest controller
 *
 */
class MeRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var \Symfony\Component\BrowserKit\Client
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var EntityManager
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {
        $this->client = static::createClient();

        static::$kernel = static::createKernel();
        static::$kernel->boot();

        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');

        $this->em = static::$kernel->getContainer() ->get('doctrine') ->getEntityManager();

        $this->setAuthenticatedUser('trainer1');

    }

    private function setAuthenticatedUser($username){
        $user = $this->userManager->findUserByUsername($username);

        /*
         * Creation of the browser client with the trainer1 authenticated header
        */
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                    'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                    'HTTP_X-WSSE' => $token,
                    'HTTP_ACCEPT' => 'application/json'
            );
            $this->client->setServerParameters($this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    public function testGetMeWithValidCredentials(){

        $this->setAuthenticatedUser('trainer1');

        $this->client->request('GET', '/api/me.json', array(), array(), $this->header);
        $content = $this->client->getResponse()->getContent();
        $trainer = json_decode($content, false);

        $this->assertTrue(in_array('ROLE_TRAINER', $trainer->user->roles), "Authenticated user is a trainer");

        $this->setAuthenticatedUser('client1');

        $this->client->request('GET', '/api/me.json', array(), array(), $this->header);
        $content = $this->client->getResponse()->getContent();
        $client = json_decode($content, false);

        $this->assertTrue(in_array('ROLE_CLIENT', $client->user->roles), "Authenticated user is a client");

    }

    public function testGetMeActionWithInvalidCredentials(){

        $this->client->setServerParameters(array());
        $this->client->request('GET', '/api/me.json');
        $response = $this->client->getResponse()->getContent();
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

    }

}
