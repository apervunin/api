<?php

namespace FitFix\ApiBundle\Tests\Controller;

use Doctrine\ORM\EntityManager;

use FitFix\CoreBundle\Entity\Client;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use Symfony\Component\BrowserKit\Client as BrowserClient;

/**
 * Test class for workout rest controller
 *
 */
class WorkoutRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var BrowserClient
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var EntityManager
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {
        $this->client = static::createClient();

        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');

        $this->em = static::$kernel->getContainer() ->get('doctrine') ->getEntityManager();

        $user = $this->userManager->findUserByUsername("trainer1");

        /*
         * Creation of the browser client with the client1 authenticated header
         */
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );
            $this->client->setServerParameters($this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get workouts
     *
     * @return none
     */
    public function testGetWorkoutsAction_valid_user()
    {
        $this->client->request('GET', '/api/trainers/trainer1/workouts');
        $content = $this->client->getResponse()->getContent();
        $workouts = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(1, $workouts);
    }

    /**
     * Test get workout
     *
     * @return none
     */
    public function testGetWorkoutAction_valid_user()
    {
        $this->client->request('GET', '/api/trainers/trainer1/workouts/1');
        $content = $this->client->getResponse()->getContent();
        $workout = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertEquals('Morning', $workout->name);
    }

    /**
     * Test get workout nonexistent slug
     *
     * @return none
     */
    public function testGetWorkoutAction_invalid_client()
    {
        $this->client->request('GET', '/api/trainers/trainer0/workouts');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get workout unauthorised slug
     *
     * @return none
     */
    public function testGetWorkoutAction_unauthorised_trainer()
    {
        $this->client->request('GET', '/api/trainers/trainer2/workouts');

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test create workout
     *
     * @return none
     */
    public function testPostWorkoutsAction()
    {

        $params = array(
            'name'      => 'Evening'
        );

        $this->client->request('POST', '/api/trainers/trainer1/workouts', $params);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $this->assertRegExp('/\/api\/trainers\/trainer1\/workouts\/3/', $this->client->getResponse()->headers->get('location'));

        $workout = $this->em->getRepository('FitFixCoreBundle:Workout')->find(3);
        $this->assertNotNull($workout, "Workout created");
        if ($workout) {
            $this->em->remove($workout);
            $this->em->flush();
        }
    }

    /**
     * Test create workout invalid data
     *
     * @return none
     */
    public function testPostWorkoutsAction_invalid_data()
    {
        $params = array(
            'name'      => ''
        );

        $this->client->request('POST', '/api/trainers/trainer1/workouts', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"name":["Please enter a name"]}';
        $this->assertEquals($expected, $this->client->getResponse()->getContent());

    }

    /**
     * Test delete an existing workout
     *
     * @return none
     */
    public function testDeleteWorkoutAction()
    {
        $this->client->request('DELETE', '/api/trainers/trainer1/workouts/2');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete workout nonexistent
     *
     * @return none
     */
    public function testDeleteWorkoutAction_invalid_workout()
    {
        $this->client->request('DELETE', '/api/trainers/trainer1/workouts/0');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete workout without id
     *
     * @return none
     */
    public function testDeleteWorkoutAction_without_id()
    {
        $this->client->request('DELETE', '/api/trainers/trainer1/workouts');

        $this->assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update workout
     *
     * @return none
     */
    public function testPutWorkoutAction()
    {
        $params = array('name' => 'Weights');

        $this->client->request('PUT', '/api/trainers/trainer1/workouts/1', $params);

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }

    /**
     * Test update workout invalid id
     *
     * @return none
     */
    public function testPutWorkoutAction_invalid_id()
    {
        $params = array('name' => 'Weights');

        $this->client->request('PUT', '/api/trainers/trainer1/workouts/0', $params);

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update workout unauthorised slug
     *
     * @return none
     */
    public function testPutWorkoutAction_unauthorised_trainer()
    {
        $params = array('name' => 'Weights');

        $this->client->request('GET', '/api/trainers/trainer2/workouts/1', $params);

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get options
     *
     * @return none
     */
    public function testOptionsWorkoutsAction()
    {
        $this->client->request('OPTIONS', '/api/trainer/workouts', array(), array(), $this->header);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('access-control-allow-methods', 'OPTIONS, GET, POST, PUT, DELETE'));
    }
}
