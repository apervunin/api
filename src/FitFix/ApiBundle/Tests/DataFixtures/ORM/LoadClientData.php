<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Client;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadClientData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // client-1
        $client = new Client();
        $client->setTrainer($this->getReference('trainer-1'));
        $client->setGender('male');
        $client->setMobile('123456789');
        $client->setDob(new DateTime('1980-12-13'));
        $client->setFirstName('John');
        $client->setLastName('Smith');
        $client->setPhoto('me.jpg');

        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user->setUsername('client1');
        $user->setEmail('client1@test.com');
        $user->setPlainPassword('111111');
        $user->addRole('role_client');
        $user->setRegistered(new \DateTime());

        $user->setClient($client);
        $client->setUser($user);

        $userManager->updateUser($user);

        $manager->persist($user);

        $this->addReference('client-1', $client);

        // client-2
        $client = new Client();
        $client->setTrainer($this->getReference('trainer-1'));
        $client->setGender('female');
        $client->setMobile('123456789');
        $client->setDob(new DateTime('1970-01-31'));
        $client->setFirstName('Mary');
        $client->setLastName('Jones');
        $client->setPhoto('me.jpg');

        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user->setUsername('client2');
        $user->setEmail('client2@test.com');
        $user->setPlainPassword('111111');
        $user->addRole('role_client');
        $user->setRegistered(new \DateTime());

        $user->setClient($client);
        $client->setUser($user);

        $userManager->updateUser($user);

        $manager->persist($user);

        $this->addReference('client-2', $client);

        // client-3
        $client = new Client();
        $client->setTrainer($this->getReference('trainer-1'));
        $client->setGender('female');
        $client->setMobile('123456789');
        $client->setDob(new DateTime('1970-01-31'));
        $client->setFirstName('Pia');
        $client->setLastName('Williams');
        $client->setPhoto('me.jpg');

        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user->setUsername('client3');
        $user->setEmail('client3@test.com');
        $user->setPlainPassword('111111');
        $user->addRole('role_client');
        $user->setRegistered(new \DateTime());

        $user->setClient($client);
        $client->setUser($user);

        $userManager->updateUser($user);

        $manager->persist($user);

        $this->addReference('client-3', $client);

        $manager->flush();

    }

    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }

}
