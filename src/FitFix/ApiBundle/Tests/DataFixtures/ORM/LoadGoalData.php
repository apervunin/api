<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Goal;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use \DateTime;

class LoadGoalData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // goal-1
        $goal = new Goal();
        $goal->setClient($this->getReference('client-1'));
        $goal->setDescription('Get Fit');

        $manager->persist($goal);

        // goal-2
        $goal = new Goal();
        $goal->setClient($this->getReference('client-2'));
        $goal->setDescription('Keep Fit');

        $manager->persist($goal);

        $manager->flush();

    }

    public function getOrder()
    {
        return 3; // the order in which fixtures will be loaded
    }

}
