<?php


namespace FitFix\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Package
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FitFix\CoreBundle\Repository\PackageRepository")
 * @ExclusionPolicy("all")
 */
class Package
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"package-details", "package-list", "packagepurchase-details", "packagepurchase-list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Expose
     * @Groups({"package-details", "package-list", "packagepurchase-details", "packagepurchase-list"})
     * 
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Expose
     * @Groups({"package-details", "package-list", "packagepurchase-details"})
     * 
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="text")
     * @Expose
     * @Groups({"package-details", "package-list", "packagepurchase-details", "packagepurchase-list"})
     * 
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2)
     * @Expose
     * @Groups({"package-details", "package-list", "packagepurchase-details", "packagepurchase-list"})
     * 
     * @Assert\GreaterThan(value=0)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $price;

    /**
     *
     * @ORM\ManyToOne(targetEntity="FitFix\CoreBundle\Entity\Trainer", inversedBy="packages")
     * @ORM\JoinColumn(name="trainer_id", referencedColumnName="id")
     * 
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $trainer;

    /**
     * @var integer
     *
     * @ORM\Column(name="numberOfSessions", type="smallint")
     * @Expose
     * @Groups({"package-details", "package-list", "packagepurchase-details", "packagepurchase-list"})
     * 
     * @SerializedName("numberOfSessions")
     * 
     * @Assert\GreaterThan(value=0)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $numberOfSessions;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     * @Expose()
     * @Groups({"package-details"})
     */
    private $created;
    
    public function __toString(){
    	return $this->getName();
    }

    public function __construct(){
    	$this->setCreated(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Package
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Package
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

     /**
     * Set type
     *
     * @param string $type
     * @return Package
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Package
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set numberOfSessions
     *
     * @param integer $numberOfSessions
     * @return Package
     */
    public function setNumberOfSessions($numberOfSessions)
    {
        $this->numberOfSessions = $numberOfSessions;
    
        return $this;
    }

    /**
     * Get numberOfSessions
     *
     * @return integer 
     */
    public function getNumberOfSessions()
    {
        return $this->numberOfSessions;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Package
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get trainer
     *
     * @return \FitFix\CoreBundle\Entity\Trainer 
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

     /**
     * Set trainer
     *
     * @param \FitFix\CoreBundle\Entity\Trainer $trainer
     * @return Package
     */
    public function setTrainer(\FitFix\CoreBundle\Entity\Trainer $trainer = null)
    {
        $this->trainer = $trainer;

        return $this;
    }

}