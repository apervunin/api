<?php

namespace FitFix\ApiBundle\Tests\Controller;

use FitFix\CoreBundle\Entity\Client;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test class for address rest controller
 *
 */
class AddressRestControllerTest extends WebTestCase
{

    /**
     * Browser Client
     * @var type
     */
    private $client;

    /**
     * Service Container  fos_user.user_manager
     * @var type
     */
    private $userManager;

    /**
     * Authentication header
     * @var type
     */
    private $header;

    /**
     * Entity Manager
     * @var type
     */
    private $em;

    /**
     * Test environment setup
     *
     * @return none
     */
    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $this->userManager = $kernel->getContainer()->get('fos_user.user_manager');

        $this->em = $kernel->getContainer() ->get('doctrine') ->getEntityManager();

        /*
         * Creation of the browser client with the trainer1 authenticated header
         */
        $user = $this->userManager->findUserByUsername("trainer1");
        if ($user) {
            $username = $user->getUsername();
            $password = $user->getPassword();
            $created = date('c');
            $nonce = substr(md5(uniqid('nonce_', true)), 0, 16);
            $nonceSixtyFour = base64_encode($nonce);
            $passwordDigest = base64_encode(sha1($nonce . $created . $password, true));
            $token = "UsernameToken Username=\"{$username}\", PasswordDigest=\"{$passwordDigest}\", Nonce=\"{$nonceSixtyFour}\", Created=\"{$created}\"";
            $this->header = array(
                'HTTP_Authorization' => 'WSSE profile="UsernameToken"',
                'HTTP_X-WSSE' => $token,
                'HTTP_ACCEPT' => 'application/json'
            );
            $this->client = static::createClient(array(), $this->header);
        }
    }

    /**
     * Closes the Doctrine EM connection
     */
    public function tearDown() {
        $this->em->getConnection()->close();
        parent::tearDown();
    }

    /**
     * Test get addresses
     *
     * @return none
     */
    public function testGetAddressesAction()
    {
        $this->client->request('GET', '/api/addresses');
        $content = $this->client->getResponse()->getContent();
        $addresses = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertCount(3, $addresses);
    }

    /**
     * Test get address
     *
     * @return none
     */
    public function testGetAddressAction()
    {
        $this->client->request('GET', '/api/addresses/1');
        $content = $this->client->getResponse()->getContent();
        $address = json_decode($content, false);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $this->assertEquals('Gym', $address->name);
    }

    /**
     * Test create address
     *
     * @return none
     */
    public function testPostAddressesAction()
    {

        $params = array(
            'name'      => 'Leisure Centre',
            'address1'  => '1 Forest Road',
            'address2'  => 'Hawkenbury',
            'town'      => 'Tunbridge Wells',
            'county'    => 'Kent',
            'postcode'  => 'TN2 1XY',
            'country'   => 'GB',
            'longitude' => 51.129195,
            'latitude'  => 0.262878
        );

        $this->client->request('POST', '/api/addresses', $params);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $this->assertRegExp('/\/api\/addresses\/4/', $this->client->getResponse()->headers->get('location'));

        $address = $this->em->getRepository('FitFixCoreBundle:Address')->find(4);
        $this->assertNotNull($address, "Address created");
        if ($address) {
            $this->em->remove($address);
            $this->em->flush();
        }
    }

    /**
     * Test create address invalid data
     *
     * @return none
     */
    public function testPostAddressesAction_invalid_data()
    {
        $params = array(
            'name'      => '',
            'longitude' => 'sometext',
            'latitude' => 'sometext'
        );

        $this->client->request('POST', '/api/addresses', $params);

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));

        $expected = '{"name":["Please enter a name"],"address1":["Please enter first line of address"],"postcode":["Please enter a post code"],"latitude":["The value sometext is not a valid numeric type."],"longitude":["The value sometext is not a valid numeric type."]}';
        $this->assertEquals($expected, $this->client->getResponse()->getContent());

    }

    /**
     * Test delete an existing address
     *
     * @return none
     */
    public function testDeleteAddressAction()
    {
        $this->client->request('DELETE', '/api/addresses/2');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete address nonexistent
     *
     * @return none
     */
    public function testDeleteAddressAction_invalid_address()
    {
        $this->client->request('DELETE', '/api/addresses/0');

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
    }

    /**
     * Test delete address without id
     *
     * @return none
     */
    public function testDeleteAddressAction_without_id()
    {
        $this->client->request('DELETE', '/api/addresses');

        $this->assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test update address
     *
     * @return none
     */
    public function testPutAddressAction()
    {
        $params = array('name' => 'Playing field');

        $this->client->request('PUT', '/api/addresses/1', $params);

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }

    /**
     * Test update address invalid id
     *
     * @return none
     */
    public function testPutAddressAction_invalid_id()
    {
        $params = array('name' => 'Playing field');

        $this->client->request('PUT', '/api/addresses/0', $params);

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test get options
     *
     * @return none
     */
    public function testOptionsAddressesAction()
    {
        $this->client->request('OPTIONS', '/api/addresses', array(), array(), $this->header);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->client->getResponse()->headers->contains('access-control-allow-methods', 'OPTIONS, GET, POST, PUT, DELETE'));
    }
}
