<?php

namespace FitFix\ApiBundle\Tests\DataFixtures\ORM;

use FitFix\CoreBundle\Entity\Trainer;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadTrainerData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // trainer-1
        $trainer = new Trainer();
        $trainer->setFirstName('Sam');
        $trainer->setLastName('Davis');
        $trainer->setPersonalDetails('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae risus vel elit pretium pulvinar in in sem. Curabitur adipiscing feugiat viverra. Vivamus et tincidunt purus.');
        $trainer->setSpecialisms('Lorem ipsum');
        $trainer->setBio('Lorem ipsum');
        $trainer->setPaypalAddress('Lorem ipsum');

        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user->setUsername('trainer1');
        $user->setEmail('trainer1@test.com');
        $user->setPlainPassword('111111');
        $user->addRole('role_trainer');
        $user->setRegistered(new \DateTime());

        $user->setTrainer($trainer);
        $trainer->setUser($user);

        $userManager->updateUser($user);
        $manager->persist($trainer);

        $this->addReference('trainer-1', $trainer);

        // trainer-2
        $trainer = new Trainer();
        $trainer->setFirstName('Brian');
        $trainer->setLastName('Walker');
        $trainer->setPersonalDetails('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae risus vel elit pretium pulvinar in in sem. Curabitur adipiscing feugiat viverra. Vivamus et tincidunt purus.');
        $trainer->setSpecialisms('Lorem ipsum');
        $trainer->setBio('Lorem ipsum');
        $trainer->setPaypalAddress('Lorem ipsum');

        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user->setUsername('trainer2');
        $user->setEmail('trainer2@test.com');
        $user->setPlainPassword('111111');
        $user->addRole('role_trainer');
        $user->setRegistered(new \DateTime());

        $user->setTrainer($trainer);
        $trainer->setUser($user);

        $userManager->updateUser($user);
        $manager->persist($trainer);

        $this->addReference('trainer-2', $trainer);

        $manager->flush();

    }

    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }

}
