<?php

namespace FitFix\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class StringToDateTimeTransformer implements DataTransformerInterface {
	
	/**
	 * (non-PHPdoc)
	 * @see \Symfony\Component\Form\DataTransformerInterface::transform()
	 */
	public function transform($date){

		if(!$date){
			return null;
		}
		
		return $date->getTimestamp();
		
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \Symfony\Component\Form\DataTransformerInterface::reverseTransform()
	 */
	public function reverseTransform($date){
				
		$date = strtotime($date);
		
		if($date === false){
			return null;
		}
		
		return new \DateTime("@" . $date);
	}
	
}