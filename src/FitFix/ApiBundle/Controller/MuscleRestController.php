<?php

namespace FitFix\ApiBundle\Controller;

use FitFix\CoreBundle\Entity\Goal;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View AS FOSView;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\Serializer\SerializationContext;

use \DateTime;

/**
 * Controller that provides Restful services over the resource Muscle.
 *
 * @NamePrefix("fitfix_api_musclerest_")
 * @author Patrick Lock <patrick.lock@gmail.com>
 */
class MuscleRestController extends Controller
{

    /**
     * Returns all muscles.
     *
     * @return FOSView
     * @Secure(roles="ROLE_CLIENT, ROLE_TRAINER")
     * @ApiDoc()
     */
    public function getMusclesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $view = FOSView::create();

        $entities = $em->getRepository('FitFixCoreBundle:Muscle')->findAll();

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Muscle entities.');
        }

        if ($entities) {
            $view->setSerializationContext(SerializationContext::create()->setGroups(array("list")));
            $view->setStatusCode(200)->setData($entities);
        } else {
            $view->setStatusCode(404);
        }

        return $view;
    }
}
