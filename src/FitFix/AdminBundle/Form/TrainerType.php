<?php

namespace FitFix\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TrainerType extends AbstractType
{
    public function __construct($mode = 'update') {
        $this->mode = $mode;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', new UserType())
            ->add('firstName')
            ->add('lastName')
            ->add('personalDetails')
            ->add('specialisms')
            ->add('bio')
            ->add('clients', 'entity', array(
                'class' => 'FitFixCoreBundle:Client',
                'multiple' => true
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        if ($this->mode == 'add') {
            $validationGroups = array('Registration', 'all');
        }
        elseif ($this->mode == 'update') {
            $validationGroups = array('Profile', 'all');
        }

        $resolver->setDefaults(array(
            'data_class' => 'FitFix\CoreBundle\Entity\Trainer',
            'validation_groups' => $validationGroups
        ));
    }

    public function getName()
    {
        return 'fitfix_adminbundle_trainertype';
    }
}
