<?php

namespace FitFix\FrontendBundle\Form;

use FitFix\AdminBundle\Form\UserType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FitFix\CoreBundle\Form\AddressType;

class TrainerType extends AbstractType
{
    public function __construct($mode = 'update') {
        $this->mode = $mode;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', new UserType())
            ->add('firstName', 'text', array(       	'attr' => array(
            	'class' => 'text',
        		'placeholder' => 'First Name'
            )))
            ->add('lastName', 'text', array(       	'attr' => array(
            	'class' => 'text',
        		'placeholder' => 'Last Name'
            )))
            ->add('gender', 'choice', array(
        		'choices' => array(
            		'Male' => 'Male',
        			'Female' => 'Female'
            ),
            		'attr' => array(
        	'class' => 'select',
            				'onchange' => 'get_gender();'
        )
        	))
            ->add('phone', 'text', array(       	'attr' => array(
            	'class' => 'text',
        		'placeholder' => 'Phone or Mobile Number'
            )))
            ->add('birthday', 'birthday')
            ->add('addressNumber', 'text',  array(       	'attr' => array(
            	'class' => 'text-2',
        		'placeholder' => 'No.'
            )))
            ->add('addressStreet', 'text',  array(       	'attr' => array(
            	'class' => 'text-2',
        		'placeholder' => 'Street'
            )))
            ->add('addressTown', 'text',  array(       	'attr' => array(
            	'class' => 'text',
        		'placeholder' => 'Town or City'
            )))
            ->add('addressPostCode', 'text',  array(       	'attr' => array(
            	'class' => 'text-3',
        		'placeholder' => 'Post Code/Zip Code'
            )))
            ->add('addressCountry', 'country',  array(
            'choices' => array(
            	'UK' => 'UK',
            	'USA' => 'USA',
            	'Australia' => 'Australia'
            ),	
            'attr' => array(
            	'class' => 'select',
        		'onchange' => 'get_country();'
            )))
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        if ($this->mode == 'add') {
            $validationGroups = array('Registration');
        }
        elseif ($this->mode == 'update') {
            $validationGroups = array('Profile');
        }

        $resolver->setDefaults(array(
            'data_class' => 'FitFix\CoreBundle\Entity\Trainer',
            'validation_groups' => $validationGroups
        ));
    }

    public function getName()
    {
        return 'fitfix_frontendbundle_trainertype';
    }
}
